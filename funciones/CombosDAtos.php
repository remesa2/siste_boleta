<?php
include("f_usuario.php");
include("inicio.php");
validar_u();

$conn = conectar();

$sw=isset($_GET['sw']) ? $_GET['sw']:"";
$id=isset($_GET['estado']) ? $_GET['estado']:"";
$rut=isset($_GET['rut']) ? $_GET['rut']:"";
$clie=isset($_GET['cliente']) ? $_SESSION['cliente']:"";


if($sw=="cliente"){
    $consulta = "SELECT * FROM `cliente` WHERE `cli_bloqueado`=0 AND `cli_estado_doc`=1";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="" selected="selected" disabled="disabled">Seleccione</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['cli_id'].'">'.$fila['cli_nombre'].'</option>';
	};
}

if($sw=="estado"){
    $consulta = "SELECT `ac_estado` as es_id, estado.es_nombre as es_nombre  
    FROM sist_boleta.`arbol_cliente`  
    INNER JOIN sist_boleta.estado ON estado.es_id=arbol_cliente.ac_estado 
    WHERE `ac_cliente`='$clie' AND ac_estado>0 
    GROUP BY  es_id 
    ORDER BY es_nombre ASC";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="" selected="selected" disabled="disabled">Seleccione</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['es_id'].'">'.$fila['es_nombre'].'</option>';
	};
}

if($sw=="subestado"){
    $consulta = "SELECT `ac_subestado` as sub_id, subestado.sub_nombre  
    FROM sist_boleta.`arbol_cliente`  
    INNER JOIN sist_boleta.subestado ON subestado.sub_id=arbol_cliente.ac_subestado 
    WHERE `ac_estado`='$id' AND ac_cliente='$clie' 
    order by subestado.sub_nombre ASC";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="" selected="selected" disabled="disabled">Seleccione</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['sub_id'].'">'.$fila['sub_nombre'].'</option>';
	};
}

if($sw=="telefono"){
    $consulta = "SELECT * FROM sist_boleta.`telefono` 
    WHERE fono_rut='$rut'";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="0" selected="selected" disabled="disabled">Seleccione</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['fono_telefono'].'">'.$fila['fono_telefono'].'</option>';
	};
}

if($sw=="estadoRep"){
    $consulta = "SELECT `deu_estado`, estado.es_nombre   
    FROM sist_boleta.`deudor`  
    INNER JOIN sist_boleta.estado ON deudor.`deu_estado`=estado.es_id 
    WHERE `deu_cliente`='$clie' 
    GROUP BY `deu_estado`
    ORDER BY es_nombre ASC";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="" selected="selected" disabled="disabled">Todos</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['deu_estado'].'">'.$fila['es_nombre'].'</option>';
	};
    
}


if($sw=="subestadoRep"){
    $consulta = "SELECT `deu_subestado`, subestado.sub_nombre 
    FROM sist_boleta.`deudor` 
    INNER JOIN sist_boleta.subestado ON deudor.`deu_subestado`=subestado.sub_id 
    WHERE `deu_cliente`='$clie' AND `deu_estado`='$id' 
    GROUP BY `deu_subestado`
    order by subestado.sub_nombre ASC";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="" selected="selected" disabled="disabled">Todos</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['deu_subestado'].'">'.$fila['sub_nombre'].'</option>';
	};
    
}
if($sw=="mail"){
    $consulta = "SELECT * FROM sist_boleta.`mail` 
    WHERE mail_rut='$rut'";
    $query = mysqli_query($conn,$consulta);
	echo '<option value="0" selected="selected" disabled="disabled">Seleccione</option>';
	while ($fila = mysqli_fetch_array($query)) {
    echo '<option value="'.$fila['mail_correo'].'">'.$fila['mail_correo'].'</option>';
	};
}