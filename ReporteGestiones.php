<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();

$clie=$_SESSION['cliente'];

$sql_reporte="SELECT `ge_usuario`, funcionario.FU_NOMBRE, count(*) AS CANTIDAD , MIN(TIME(`ge_fecha`)) as INICIO, MAX(TIME(`ge_fecha`)) as FIN, timediff(MAX(TIME(`ge_fecha`)) , MIN(TIME(`ge_fecha`))) as HORAS, SEC_TO_TIME(TIME_TO_SEC(timediff(MAX(TIME(`ge_fecha`)) , MIN(TIME(`ge_fecha`))))/count(*)) as PROMEDIO, timediff(TIME(NOW()), MAX(TIME(`ge_fecha`))) as TIEMPO_DESDE_ULTIMA FROM sist_boleta.`gestion` INNER JOIN sist_boleta.funcionario ON gestion.ge_usuario=funcionario.FU_CODIGO WHERE `ge_cliente`=$clie and DATE(`ge_fecha`)=DATE(NOW()) GROUP BY `ge_usuario`";

//echo $sql_reporte;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-1.4.2.min.js"></script>
    <title>| Sistema Remesa</title>
      
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <a href="Export_exel_historico.php?sql=<?php echo $sql_reporte; ?>"><img src="bootstrap/img/export_to_excel.gif"> Exportar Archivo</a><br>
    <br>
    <div class="hero-unit">
       
        
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="18" class="alert-danger">Tiempos de Gestion</th>
                </tr>
                <tr class="alert-success">
                    <th>Nombre</th>
                    <th>Q Gestiones</th>
                    <th>Primera Gestion</th>
                    <th>Ultima Gestion</th>
                    <th>Horas Trabajadas</th>
                    <th>Tiempo Promedio De Gestion</th>
                    <th>Tiempo Desde la Ultima Gestion</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $conn = conectar();

                $reporte=mysqli_query($conn,$sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
                    <td><?php echo $fila->FU_NOMBRE; ?></td>
                    <td><?php echo $fila->CANTIDAD; ?></td>
                    <td><?php echo $fila->INICIO; ?></td>
                    <td><?php echo $fila->FIN; ?></td>
                    <td><?php echo $fila->HORAS; ?></td>
                    <td><?php echo $fila->PROMEDIO; ?></td>
                    <td><?php echo $fila->TIEMPO_DESDE_ULTIMA; ?></td>
                </tr>
                <?php }  ?>
                
            </tbody>
        </table> 
          
        
    </div>
</div>
</body>
</html>