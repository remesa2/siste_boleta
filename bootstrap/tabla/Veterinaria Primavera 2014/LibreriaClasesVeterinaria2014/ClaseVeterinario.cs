﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseVeterinario
    {
        protected int ID;
        protected String RUT;
        protected String Nombre;
        protected String ApPaterno;
        protected String ApMaterno;
        protected int FonoFijo;
        protected int Celular;
        protected String Email;
        protected DateTime FechaIngreso;
        protected String Especialidad;
        protected MemoryStream Foto;


        #region CONSTRUCTORES

        public ClaseVeterinario()
        {
        }

        public ClaseVeterinario(int id, String rut, String nombre, String appaterno, String apmaterno, int fonofijo, int celular, String email, DateTime fechaingreso, String especialidad, MemoryStream foto)
        {
            this.ID = id;
            this.RUT = rut;
            this.Nombre = nombre;
            this.ApPaterno = appaterno;
            this.ApMaterno = apmaterno;
            this.FonoFijo = fonofijo;
            this.Celular = celular;
            this.Email = email;
            this.FechaIngreso = fechaingreso;
            this.Especialidad = especialidad;
            this.Foto = foto;
        }

        public ClaseVeterinario(String rut, String nombre, String appaterno, String apmaterno, int fonofijo, int celular, String email, DateTime fechaingreso, String especialidad, MemoryStream foto)
        {
            this.RUT = rut;
            this.Nombre = nombre;
            this.ApPaterno = appaterno;
            this.ApMaterno = apmaterno;
            this.FonoFijo = fonofijo;
            this.Celular = celular;
            this.Email = email;
            this.FechaIngreso = fechaingreso;
            this.Especialidad = especialidad;
            this.Foto = foto;
        }

        public ClaseVeterinario(String rut, String nombre, String appaterno, String apmaterno, int fonofijo, int celular, String email, DateTime fechaingreso, String especialidad)
        {
            this.RUT = rut;
            this.Nombre = nombre;
            this.ApPaterno = appaterno;
            this.ApMaterno = apmaterno;
            this.FonoFijo = fonofijo;
            this.Celular = celular;
            this.Email = email;
            this.FechaIngreso = fechaingreso;
            this.Especialidad = especialidad;
        }

        #endregion


        #region METODOS

        public string Ingresar(ClaseVeterinario veterinario)
        {
            String sw;
            try
            {



                sw = "Datos Ingresados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public string Modificar(ClaseVeterinario veterinario)
        {
            String sw;
            try
            {



                sw = "Datos Modificados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public string Eliminar(ClaseVeterinario veterinario)
        {
            String sw;
            try
            {



                sw = "Datos Eliminados";

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        #endregion
    }
}
