﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace LibreriaClasesVeterinaria2014
{
    class ClaseConectar
    {
        public static String Dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static String DirCompleta = System.IO.Path.Combine(Dir, "BaseDatosVeterinariaPrimavera2014.mdf");

        public static string Cnn = @"Data Source=.\SQLEXPRESS;AttachDbFilename=" +DirCompleta.Remove(0,6)+";Integrated Security=True;Connect Timeout=30;User Instance=True";

        public static void Abrir()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = Cnn;
            con.Open();
        }

        public static void Cerrar()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = Cnn;
            con.Close();
        }

    }

    
}
