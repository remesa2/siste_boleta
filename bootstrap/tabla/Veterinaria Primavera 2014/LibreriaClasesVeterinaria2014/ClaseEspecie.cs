﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseEspecie
    {
        protected int ID;
        protected String Nombre;


        #region CONSTRUCTORES

        public ClaseEspecie()
        {
        }

        public ClaseEspecie(int id, String nombre)
        {
            this.ID = id;
            this.Nombre = nombre;
            
        }

        public ClaseEspecie(String nombre)
        {
            this.Nombre = nombre;
            
        }



        #endregion


        #region METODOS

        public string Ingresar(ClaseEspecie especie)
        {
            String sw;
            try
            {

                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                //cmm.CommandText = "INSERT INTO ESPECIE values (@ID_Especie, @Nombre)";
                cmm.CommandText = "INSERT INTO ESPECIE(Nombre) values (@Nombre)";
                
                //cmm.Parameters.Add("@ID_Especie", SqlDbType.Int);
                cmm.Parameters.Add("@Nombre", SqlDbType.NVarChar);

                //cmm.Parameters["@ID_Especie"].Value = on;
                cmm.Parameters["@nombre"].Value = especie.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

               

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        public string Modificar(ClaseEspecie especie)
        {
            String sw;
            try
            {


                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "UPDATE ESPECIE SET Nombre=@Nombre WHERE ID_Especie=@ID_Especie";

                cmm.Parameters.Add("@ID_Especie", SqlDbType.Int);
                cmm.Parameters.Add("@Nombre", SqlDbType.NVarChar);

                cmm.Parameters["@ID_Especie"].Value = especie.ID;
                cmm.Parameters["@nombre"].Value = especie.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public string Eliminar(ClaseEspecie especie)
        {
            String sw;
            try
            {

                SqlConnection cnn = new SqlConnection();
                cnn.ConnectionString = ClaseConectar.Cnn;
                SqlCommand cmm = new SqlCommand("", cnn);
                cmm.CommandText = "DELETE ESPECIE WHERE ID_Especie=@ID_Especie";

                cmm.Parameters.Add("@ID_Especie", SqlDbType.Int);
                cmm.Parameters.Add("@Nombre", SqlDbType.NVarChar);

                cmm.Parameters["@ID_Especie"].Value = especie.ID;
                cmm.Parameters["@nombre"].Value = especie.Nombre;

                cnn.Open();
                sw = Convert.ToString(cmm.ExecuteNonQuery());
                cnn.Close();



            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public static SqlDataAdapter llenagrilla()
        {
            SqlConnection cnn = new SqlConnection();
            cnn.ConnectionString = ClaseConectar.Cnn;
            cnn.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM ESPECIE", cnn);
            cnn.Close();

            return DA;

        }



        #endregion

    }
}
