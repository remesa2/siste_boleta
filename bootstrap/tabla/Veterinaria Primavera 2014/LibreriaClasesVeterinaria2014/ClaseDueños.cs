﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseDueños
    {
        protected int ID;
        protected String RUT;
        protected String nombre;
        protected String Appaterno;
        protected String ApMaterno;
        protected int FonoFijo;
        protected int celular;
        protected String email;
        protected DateTime fechaIngreso;
        protected int codComuna;
        protected String calleNro;



        #region CONSTRUCTORES


        public ClaseDueños()
        {
        }


        public ClaseDueños(int id,
                            String Rut,
                            String nombre,
                            String apPaterno,
                            String apMaterno,
                            int fonoFijo,
                            int celular,
                            String email,
                            DateTime fechaIngreso,
                            int comuna,
                            String calleNro)
        {
            this.ID = id;
            this.RUT = Rut;
            this.nombre = nombre;
            this.Appaterno = apPaterno;
            this.ApMaterno = apMaterno;
            this.FonoFijo = fonoFijo;
            this.celular = celular;
            this.email = email;
            this.fechaIngreso = fechaIngreso;
            this.codComuna = comuna;
            this.calleNro = calleNro;
        }



        public ClaseDueños(String Rut,
                            String nombre,
                            String apPaterno,
                            String apMaterno,
                            int fonoFijo,
                            int celular,
                            String email,
                            DateTime fechaIngreso,
                            int comuna,
                            String calleNro)
        {
            this.RUT = Rut;
            this.nombre = nombre;
            this.Appaterno = apPaterno;
            this.ApMaterno = apMaterno;
            this.FonoFijo = fonoFijo;
            this.celular = celular;
            this.email = email;
            this.fechaIngreso = fechaIngreso;
            this.codComuna = comuna;
            this.calleNro = calleNro;
        }


        #endregion



        #region METODOS

        public string Ingresar(ClaseDueños dueños)
        {
            String sw;
            try
            {



                sw = "Datos Ingresados"+dueños.RUT+ "\n"+dueños.nombre;

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }


        public string Modificar(ClaseDueños dueños)
        {
            String sw;
            try
            {



                sw = "Datos Modificados" + dueños.RUT + "\n" + dueños.nombre;

            }
            catch (Exception ex)
            {
                sw = ex.Message;
            }

            return sw;
        }

        #endregion



    }

}
