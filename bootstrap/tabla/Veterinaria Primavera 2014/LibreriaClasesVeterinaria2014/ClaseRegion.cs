﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClasesVeterinaria2014
{
    public class ClaseRegion
    {
        protected int ID;
        protected String Nombre;


        public ClaseRegion()
        {
        }

        public ClaseRegion(int ID, String Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

    }
}
