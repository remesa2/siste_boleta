﻿namespace Veterinaria_Primavera_2014
{
    partial class FormVeterinario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVeterinario));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnIngresarFoto = new System.Windows.Forms.Button();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtCelular = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtFijo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtNombres = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtApMaterno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtApPaterno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtRUT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureFoto = new System.Windows.Forms.PictureBox();
            this.btnIngresarVeterinario = new System.Windows.Forms.Button();
            this.btnModificarVeterinario = new System.Windows.Forms.Button();
            this.btnEliminarVeterinario = new System.Windows.Forms.Button();
            this.dataGridVeterinario = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.cmbEspecialidad = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimeIngreso = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVeterinario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.dateTimeIngreso);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbEspecialidad);
            this.groupBox1.Controls.Add(this.btnIngresarFoto);
            this.groupBox1.Controls.Add(this.TxtEmail);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtCelular);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TxtFijo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtNombres);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtApMaterno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtApPaterno);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtRUT);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureFoto);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1044, 169);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATOS PERSONALES DEL VETERINARIO";
            // 
            // btnIngresarFoto
            // 
            this.btnIngresarFoto.Location = new System.Drawing.Point(188, 135);
            this.btnIngresarFoto.Name = "btnIngresarFoto";
            this.btnIngresarFoto.Size = new System.Drawing.Size(132, 23);
            this.btnIngresarFoto.TabIndex = 14;
            this.btnIngresarFoto.Text = "Ingresar Foto";
            this.btnIngresarFoto.UseVisualStyleBackColor = true;
            // 
            // TxtEmail
            // 
            this.TxtEmail.Location = new System.Drawing.Point(343, 77);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(302, 20);
            this.TxtEmail.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(340, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Correo Electronico";
            // 
            // TxtCelular
            // 
            this.TxtCelular.Location = new System.Drawing.Point(188, 77);
            this.TxtCelular.Name = "TxtCelular";
            this.TxtCelular.Size = new System.Drawing.Size(132, 20);
            this.TxtCelular.TabIndex = 5;
            this.TxtCelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCelular_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(185, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Celular";
            // 
            // TxtFijo
            // 
            this.TxtFijo.Location = new System.Drawing.Point(830, 35);
            this.TxtFijo.Name = "TxtFijo";
            this.TxtFijo.Size = new System.Drawing.Size(201, 20);
            this.TxtFijo.TabIndex = 4;
            this.TxtFijo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtFijo_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(827, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefono Fijo";
            // 
            // TxtNombres
            // 
            this.TxtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtNombres.Location = new System.Drawing.Point(667, 35);
            this.TxtNombres.Name = "TxtNombres";
            this.TxtNombres.Size = new System.Drawing.Size(152, 20);
            this.TxtNombres.TabIndex = 3;
            this.TxtNombres.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtApPaterno_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(664, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombres";
            // 
            // TxtApMaterno
            // 
            this.TxtApMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApMaterno.Location = new System.Drawing.Point(507, 35);
            this.TxtApMaterno.Name = "TxtApMaterno";
            this.TxtApMaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApMaterno.TabIndex = 2;
            this.TxtApMaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtApPaterno_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(504, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Apellido Materno";
            // 
            // TxtApPaterno
            // 
            this.TxtApPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApPaterno.Location = new System.Drawing.Point(343, 35);
            this.TxtApPaterno.Name = "TxtApPaterno";
            this.TxtApPaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApPaterno.TabIndex = 1;
            this.TxtApPaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtApPaterno_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Apellido Paterno";
            // 
            // TxtRUT
            // 
            this.TxtRUT.Location = new System.Drawing.Point(188, 35);
            this.TxtRUT.Name = "TxtRUT";
            this.TxtRUT.Size = new System.Drawing.Size(132, 20);
            this.TxtRUT.TabIndex = 0;
            this.TxtRUT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtRUT_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RUT";
            // 
            // pictureFoto
            // 
            this.pictureFoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureFoto.Image = global::Veterinaria_Primavera_2014.Properties.Resources.user91_2_;
            this.pictureFoto.Location = new System.Drawing.Point(18, 19);
            this.pictureFoto.Name = "pictureFoto";
            this.pictureFoto.Size = new System.Drawing.Size(143, 139);
            this.pictureFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureFoto.TabIndex = 0;
            this.pictureFoto.TabStop = false;
            // 
            // btnIngresarVeterinario
            // 
            this.btnIngresarVeterinario.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarVeterinario.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarVeterinario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarVeterinario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarVeterinario.Location = new System.Drawing.Point(12, 198);
            this.btnIngresarVeterinario.Name = "btnIngresarVeterinario";
            this.btnIngresarVeterinario.Size = new System.Drawing.Size(215, 39);
            this.btnIngresarVeterinario.TabIndex = 8;
            this.btnIngresarVeterinario.Text = "Ingresar Veterinario";
            this.btnIngresarVeterinario.UseVisualStyleBackColor = false;
            this.btnIngresarVeterinario.Click += new System.EventHandler(this.btnIngresarVeterinario_Click);
            // 
            // btnModificarVeterinario
            // 
            this.btnModificarVeterinario.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificarVeterinario.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificarVeterinario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificarVeterinario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarVeterinario.Location = new System.Drawing.Point(233, 198);
            this.btnModificarVeterinario.Name = "btnModificarVeterinario";
            this.btnModificarVeterinario.Size = new System.Drawing.Size(224, 39);
            this.btnModificarVeterinario.TabIndex = 9;
            this.btnModificarVeterinario.Text = "Modificar Datos del Veterinario";
            this.btnModificarVeterinario.UseVisualStyleBackColor = false;
            this.btnModificarVeterinario.Click += new System.EventHandler(this.btnModificarVeterinario_Click);
            // 
            // btnEliminarVeterinario
            // 
            this.btnEliminarVeterinario.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminarVeterinario.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminarVeterinario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminarVeterinario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarVeterinario.Location = new System.Drawing.Point(842, 198);
            this.btnEliminarVeterinario.Name = "btnEliminarVeterinario";
            this.btnEliminarVeterinario.Size = new System.Drawing.Size(212, 39);
            this.btnEliminarVeterinario.TabIndex = 10;
            this.btnEliminarVeterinario.Text = "Eliminar Veterinario";
            this.btnEliminarVeterinario.UseVisualStyleBackColor = false;
            this.btnEliminarVeterinario.Click += new System.EventHandler(this.btnEliminarVeterinario_Click);
            // 
            // dataGridVeterinario
            // 
            this.dataGridVeterinario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridVeterinario.Location = new System.Drawing.Point(12, 278);
            this.dataGridVeterinario.Name = "dataGridVeterinario";
            this.dataGridVeterinario.Size = new System.Drawing.Size(1044, 177);
            this.dataGridVeterinario.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Info;
            this.button1.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.add107__1_;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(548, 198);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 39);
            this.button1.TabIndex = 12;
            this.button1.Text = "Incluir en Atencion";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // cmbEspecialidad
            // 
            this.cmbEspecialidad.FormattingEnabled = true;
            this.cmbEspecialidad.Location = new System.Drawing.Point(667, 77);
            this.cmbEspecialidad.Name = "cmbEspecialidad";
            this.cmbEspecialidad.Size = new System.Drawing.Size(364, 21);
            this.cmbEspecialidad.TabIndex = 15;
            this.cmbEspecialidad.Text = "<<Elija>>";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(664, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Especialidad";
            // 
            // dateTimeIngreso
            // 
            this.dateTimeIngreso.Location = new System.Drawing.Point(667, 118);
            this.dateTimeIngreso.Name = "dateTimeIngreso";
            this.dateTimeIngreso.Size = new System.Drawing.Size(364, 20);
            this.dateTimeIngreso.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(664, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Fecha de Ingreso";
            // 
            // FormVeterinario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 467);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridVeterinario);
            this.Controls.Add(this.btnEliminarVeterinario);
            this.Controls.Add(this.btnModificarVeterinario);
            this.Controls.Add(this.btnIngresarVeterinario);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormVeterinario";
            this.Text = "FORMULARIO MANTENCION DE VETERINARIOS";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVeterinario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIngresarFoto;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtCelular;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtFijo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtNombres;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtApMaterno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtApPaterno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtRUT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureFoto;
        private System.Windows.Forms.Button btnIngresarVeterinario;
        private System.Windows.Forms.Button btnModificarVeterinario;
        private System.Windows.Forms.Button btnEliminarVeterinario;
        private System.Windows.Forms.DataGridView dataGridVeterinario;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimeIngreso;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbEspecialidad;
    }
}