﻿namespace Veterinaria_Primavera_2014
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ingresoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dueñosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mascotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AtenciontoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.veterinariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.parametrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.especieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.coloresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDueños = new System.Windows.Forms.ToolStripButton();
            this.toolStripMascotas = new System.Windows.Forms.ToolStripButton();
            this.toolStripAtencion = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripVeterinarios = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripAyuda = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSalir = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ayudaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.acercaDeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyInicio = new System.Windows.Forms.NotifyIcon(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresoToolStripMenuItem,
            this.mantenedoresToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.ingresoToolStripMenuItem;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(781, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ingresoToolStripMenuItem
            // 
            this.ingresoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dueñosToolStripMenuItem,
            this.mascotasToolStripMenuItem,
            this.AtenciontoolStripMenuItem,
            this.toolStripSeparator1,
            this.salirToolStripMenuItem});
            this.ingresoToolStripMenuItem.Name = "ingresoToolStripMenuItem";
            this.ingresoToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.ingresoToolStripMenuItem.Text = "&Ingreso";
            // 
            // dueñosToolStripMenuItem
            // 
            this.dueñosToolStripMenuItem.Image = global::Veterinaria_Primavera_2014.Properties.Resources.user91;
            this.dueñosToolStripMenuItem.Name = "dueñosToolStripMenuItem";
            this.dueñosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.dueñosToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.dueñosToolStripMenuItem.Text = "&Dueños";
            this.dueñosToolStripMenuItem.Click += new System.EventHandler(this.dueñosToolStripMenuItem_Click);
            // 
            // mascotasToolStripMenuItem
            // 
            this.mascotasToolStripMenuItem.Image = global::Veterinaria_Primavera_2014.Properties.Resources.dog50;
            this.mascotasToolStripMenuItem.Name = "mascotasToolStripMenuItem";
            this.mascotasToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.mascotasToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.mascotasToolStripMenuItem.Text = "&Mascotas";
            this.mascotasToolStripMenuItem.Click += new System.EventHandler(this.mascotasToolStripMenuItem_Click);
            // 
            // AtenciontoolStripMenuItem
            // 
            this.AtenciontoolStripMenuItem.Image = global::Veterinaria_Primavera_2014.Properties.Resources.add107__1_;
            this.AtenciontoolStripMenuItem.Name = "AtenciontoolStripMenuItem";
            this.AtenciontoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.AtenciontoolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AtenciontoolStripMenuItem.Text = "&Atencion";
            this.AtenciontoolStripMenuItem.Click += new System.EventHandler(this.AtenciontoolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(166, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Image = global::Veterinaria_Primavera_2014.Properties.Resources.door9_1_;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.salirToolStripMenuItem.Text = "&Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // mantenedoresToolStripMenuItem
            // 
            this.mantenedoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.veterinariosToolStripMenuItem1,
            this.toolStripSeparator7,
            this.parametrosToolStripMenuItem});
            this.mantenedoresToolStripMenuItem.Name = "mantenedoresToolStripMenuItem";
            this.mantenedoresToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.mantenedoresToolStripMenuItem.Text = "&Mantenedores";
            // 
            // veterinariosToolStripMenuItem1
            // 
            this.veterinariosToolStripMenuItem1.Name = "veterinariosToolStripMenuItem1";
            this.veterinariosToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.veterinariosToolStripMenuItem1.Text = "&veterinarios";
            this.veterinariosToolStripMenuItem1.Click += new System.EventHandler(this.veterinariosToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(132, 6);
            // 
            // parametrosToolStripMenuItem
            // 
            this.parametrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.especieToolStripMenuItem,
            this.razasToolStripMenuItem,
            this.toolStripSeparator8,
            this.coloresToolStripMenuItem});
            this.parametrosToolStripMenuItem.Name = "parametrosToolStripMenuItem";
            this.parametrosToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.parametrosToolStripMenuItem.Text = "&parametros";
            // 
            // especieToolStripMenuItem
            // 
            this.especieToolStripMenuItem.Name = "especieToolStripMenuItem";
            this.especieToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.especieToolStripMenuItem.Text = "Especies";
            this.especieToolStripMenuItem.Click += new System.EventHandler(this.especieToolStripMenuItem_Click);
            // 
            // razasToolStripMenuItem
            // 
            this.razasToolStripMenuItem.Name = "razasToolStripMenuItem";
            this.razasToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.razasToolStripMenuItem.Text = "Razas";
            this.razasToolStripMenuItem.Click += new System.EventHandler(this.razasToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(115, 6);
            // 
            // coloresToolStripMenuItem
            // 
            this.coloresToolStripMenuItem.Name = "coloresToolStripMenuItem";
            this.coloresToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.coloresToolStripMenuItem.Text = "Colores";
            this.coloresToolStripMenuItem.Click += new System.EventHandler(this.coloresToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ayudaToolStripMenuItem1,
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "&Ayuda";
            // 
            // ayudaToolStripMenuItem1
            // 
            this.ayudaToolStripMenuItem1.Image = global::Veterinaria_Primavera_2014.Properties.Resources.help;
            this.ayudaToolStripMenuItem1.Name = "ayudaToolStripMenuItem1";
            this.ayudaToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.ayudaToolStripMenuItem1.Size = new System.Drawing.Size(233, 22);
            this.ayudaToolStripMenuItem1.Text = "&Ayuda";
            this.ayudaToolStripMenuItem1.Click += new System.EventHandler(this.ayudaToolStripMenuItem1_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F10)));
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.acercaDeToolStripMenuItem.Text = "A&cerca de...";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDueños,
            this.toolStripMascotas,
            this.toolStripAtencion,
            this.toolStripSeparator2,
            this.toolStripVeterinarios,
            this.toolStripSeparator3,
            this.toolStripAyuda,
            this.toolStripSeparator4,
            this.toolStripSalir});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(781, 25);
            this.toolStrip1.TabIndex = 5;
            // 
            // toolStripDueños
            // 
            this.toolStripDueños.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDueños.Image = global::Veterinaria_Primavera_2014.Properties.Resources.user91_2_;
            this.toolStripDueños.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDueños.Name = "toolStripDueños";
            this.toolStripDueños.Size = new System.Drawing.Size(23, 22);
            this.toolStripDueños.Text = "Ingreso / Consulta Dueños";
            this.toolStripDueños.Click += new System.EventHandler(this.dueñosToolStripMenuItem_Click);
            // 
            // toolStripMascotas
            // 
            this.toolStripMascotas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripMascotas.Image = global::Veterinaria_Primavera_2014.Properties.Resources.dog50__1_;
            this.toolStripMascotas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripMascotas.Name = "toolStripMascotas";
            this.toolStripMascotas.Size = new System.Drawing.Size(23, 22);
            this.toolStripMascotas.Text = "Ingreso / Consulta de Mascotas";
            this.toolStripMascotas.Click += new System.EventHandler(this.mascotasToolStripMenuItem_Click);
            // 
            // toolStripAtencion
            // 
            this.toolStripAtencion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAtencion.Image = global::Veterinaria_Primavera_2014.Properties.Resources.add107__1_;
            this.toolStripAtencion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAtencion.Name = "toolStripAtencion";
            this.toolStripAtencion.Size = new System.Drawing.Size(23, 22);
            this.toolStripAtencion.Text = "toolStripButton1";
            this.toolStripAtencion.Click += new System.EventHandler(this.AtenciontoolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripVeterinarios
            // 
            this.toolStripVeterinarios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripVeterinarios.Image = ((System.Drawing.Image)(resources.GetObject("toolStripVeterinarios.Image")));
            this.toolStripVeterinarios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripVeterinarios.Name = "toolStripVeterinarios";
            this.toolStripVeterinarios.Size = new System.Drawing.Size(23, 22);
            this.toolStripVeterinarios.Text = "Ingreso/ Consulta de Veterinarios";
            this.toolStripVeterinarios.Click += new System.EventHandler(this.veterinariosToolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripAyuda
            // 
            this.toolStripAyuda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAyuda.Image = global::Veterinaria_Primavera_2014.Properties.Resources.help;
            this.toolStripAyuda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAyuda.Name = "toolStripAyuda";
            this.toolStripAyuda.Size = new System.Drawing.Size(23, 22);
            this.toolStripAyuda.Text = "Ayuda";
            this.toolStripAyuda.Click += new System.EventHandler(this.toolStripAyuda_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSalir
            // 
            this.toolStripSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSalir.Image = global::Veterinaria_Primavera_2014.Properties.Resources.door9_1_;
            this.toolStripSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSalir.Name = "toolStripSalir";
            this.toolStripSalir.Size = new System.Drawing.Size(23, 22);
            this.toolStripSalir.Text = "Salir de la Aplicacion";
            this.toolStripSalir.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator5,
            this.ayudaToolStripMenuItem2,
            this.toolStripSeparator6,
            this.acercaDeToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(235, 82);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(231, 6);
            // 
            // ayudaToolStripMenuItem2
            // 
            this.ayudaToolStripMenuItem2.Name = "ayudaToolStripMenuItem2";
            this.ayudaToolStripMenuItem2.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.ayudaToolStripMenuItem2.Size = new System.Drawing.Size(234, 22);
            this.ayudaToolStripMenuItem2.Text = "Ayuda";
            this.ayudaToolStripMenuItem2.Click += new System.EventHandler(this.ayudaToolStripMenuItem1_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(231, 6);
            // 
            // acercaDeToolStripMenuItem1
            // 
            this.acercaDeToolStripMenuItem1.Name = "acercaDeToolStripMenuItem1";
            this.acercaDeToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F11)));
            this.acercaDeToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.acercaDeToolStripMenuItem1.Text = "Acerca De...";
            this.acercaDeToolStripMenuItem1.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // notifyInicio
            // 
            this.notifyInicio.BalloonTipText = "Aplicacion Veterinaria Ha sido Iniciada.";
            this.notifyInicio.ContextMenuStrip = this.contextMenuStrip2;
            this.notifyInicio.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyInicio.Icon")));
            this.notifyInicio.Text = "notifyIcon1";
            this.notifyInicio.Visible = true;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 521);
            this.ContextMenuStrip = this.contextMenuStrip2;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "VETERINARIA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ingresoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dueñosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mascotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mantenedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem veterinariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem parametrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AtenciontoolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripDueños;
        private System.Windows.Forms.ToolStripButton toolStripMascotas;
        private System.Windows.Forms.ToolStripButton toolStripAtencion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripVeterinarios;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripAyuda;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripSalir;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem1;
        private System.Windows.Forms.NotifyIcon notifyInicio;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem especieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem coloresToolStripMenuItem;
    }
}

