﻿namespace Veterinaria_Primavera_2014
{
    partial class FormAtencion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAtencion));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtFijo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbRaza = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbEspecie = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNombreMascota = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridAtenciones = new System.Windows.Forms.DataGridView();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnIngresarAtencion = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dtmpFechaAtencion = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtID_Mascota = new System.Windows.Forms.TextBox();
            this.txtID_Veterinario = new System.Windows.Forms.TextBox();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAtenciones)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtID_Mascota);
            this.groupBox2.Controls.Add(this.TxtFijo);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbRaza);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cmbEspecie);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtNombreMascota);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(624, 106);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATOS DE LA MASCOTA";
            // 
            // TxtFijo
            // 
            this.TxtFijo.Location = new System.Drawing.Point(18, 75);
            this.TxtFijo.Name = "TxtFijo";
            this.TxtFijo.Size = new System.Drawing.Size(138, 20);
            this.TxtFijo.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Telefono Fijo del Dueño";
            // 
            // cmbRaza
            // 
            this.cmbRaza.FormattingEnabled = true;
            this.cmbRaza.Location = new System.Drawing.Point(469, 34);
            this.cmbRaza.Name = "cmbRaza";
            this.cmbRaza.Size = new System.Drawing.Size(138, 21);
            this.cmbRaza.TabIndex = 14;
            this.cmbRaza.Text = "<<Elija>>";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(466, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Raza";
            // 
            // cmbEspecie
            // 
            this.cmbEspecie.FormattingEnabled = true;
            this.cmbEspecie.Location = new System.Drawing.Point(305, 34);
            this.cmbEspecie.Name = "cmbEspecie";
            this.cmbEspecie.Size = new System.Drawing.Size(138, 21);
            this.cmbEspecie.TabIndex = 12;
            this.cmbEspecie.Text = "<<Elija>>";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(302, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Especie";
            // 
            // txtNombreMascota
            // 
            this.txtNombreMascota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreMascota.Location = new System.Drawing.Point(18, 35);
            this.txtNombreMascota.Name = "txtNombreMascota";
            this.txtNombreMascota.Size = new System.Drawing.Size(264, 20);
            this.txtNombreMascota.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Nombre Mascota";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Diagnostico";
            // 
            // dataGridAtenciones
            // 
            this.dataGridAtenciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAtenciones.Location = new System.Drawing.Point(12, 428);
            this.dataGridAtenciones.Name = "dataGridAtenciones";
            this.dataGridAtenciones.Size = new System.Drawing.Size(624, 99);
            this.dataGridAtenciones.TabIndex = 6;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(15, 194);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(621, 81);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(15, 296);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(621, 81);
            this.richTextBox2.TabIndex = 9;
            this.richTextBox2.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Receta";
            // 
            // btnIngresarAtencion
            // 
            this.btnIngresarAtencion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarAtencion.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarAtencion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarAtencion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarAtencion.Location = new System.Drawing.Point(12, 383);
            this.btnIngresarAtencion.Name = "btnIngresarAtencion";
            this.btnIngresarAtencion.Size = new System.Drawing.Size(237, 39);
            this.btnIngresarAtencion.TabIndex = 10;
            this.btnIngresarAtencion.Text = "Ingresar Atencion";
            this.btnIngresarAtencion.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Buscar Veterinario";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(111, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(47, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "....";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(177, 146);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 20);
            this.textBox1.TabIndex = 13;
            // 
            // dtmpFechaAtencion
            // 
            this.dtmpFechaAtencion.Location = new System.Drawing.Point(408, 147);
            this.dtmpFechaAtencion.Name = "dtmpFechaAtencion";
            this.dtmpFechaAtencion.Size = new System.Drawing.Size(241, 20);
            this.dtmpFechaAtencion.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Fecha de Atencion";
            // 
            // txtID_Mascota
            // 
            this.txtID_Mascota.Location = new System.Drawing.Point(253, 13);
            this.txtID_Mascota.Name = "txtID_Mascota";
            this.txtID_Mascota.Size = new System.Drawing.Size(29, 20);
            this.txtID_Mascota.TabIndex = 20;
            // 
            // txtID_Veterinario
            // 
            this.txtID_Veterinario.Location = new System.Drawing.Point(373, 168);
            this.txtID_Veterinario.Name = "txtID_Veterinario";
            this.txtID_Veterinario.Size = new System.Drawing.Size(29, 20);
            this.txtID_Veterinario.TabIndex = 21;
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(536, 393);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(100, 20);
            this.txtMonto.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(422, 396);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Monto de la Atencion";
            // 
            // FormAtencion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 548);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.txtID_Veterinario);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtmpFechaAtencion);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnIngresarAtencion);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.dataGridAtenciones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormAtencion";
            this.Text = "FORMULARIO DE ATENCION";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAtenciones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbRaza;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbEspecie;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtNombreMascota;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtFijo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridAtenciones;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnIngresarAtencion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtID_Mascota;
        private System.Windows.Forms.DateTimePicker dtmpFechaAtencion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtID_Veterinario;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label label6;
    }
}