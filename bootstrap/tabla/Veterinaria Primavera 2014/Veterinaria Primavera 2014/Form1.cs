﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibreriaClasesVeterinaria2014;

namespace Veterinaria_Primavera_2014
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void dueñosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDueños dueños = new FormDueños();
            dueños.MdiParent = this;
            dueños.Show();
            dueños.WindowState = FormWindowState.Minimized;
            dueños.WindowState = FormWindowState.Maximized;
            //inhabilitamos los botones
            this.dueñosToolStripMenuItem.Enabled = false;
            this.toolStripDueños.Enabled = false;
            //asignamos un putero para que se delege la accion con el evento
            dueños.FormClosed += delegate { this.dueñosToolStripMenuItem.Enabled = true; this.toolStripDueños.Enabled = true; };
        }

        private void mascotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormMascota mascota = new FormMascota();
            mascota.MdiParent = this;
            mascota.Show();
            mascota.WindowState = FormWindowState.Minimized;
            mascota.WindowState = FormWindowState.Maximized;

            this.mascotasToolStripMenuItem.Enabled = false;
            this.toolStripMascotas.Enabled = false;

            mascota.FormClosed += delegate { this.mascotasToolStripMenuItem.Enabled = true; this.toolStripMascotas.Enabled = true; };

        }

        private void AtenciontoolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAtencion atencion = new FormAtencion();
            atencion.MdiParent = this;
            atencion.Show();

            atencion.WindowState = FormWindowState.Minimized;
            atencion.WindowState = FormWindowState.Maximized;

            this.AtenciontoolStripMenuItem.Enabled = false;
            this.toolStripAtencion.Enabled = false;

            atencion.FormClosed += delegate { this.AtenciontoolStripMenuItem.Enabled = true; this.toolStripAtencion.Enabled = true; };

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea salir de la aplicación?", "SALIR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }


        private void veterinariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormVeterinario veterinario = new FormVeterinario();
            veterinario.MdiParent = this;

            veterinario.Show();
        }

       

        

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AcercaDe cuadro = new AcercaDe();
            cuadro.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.notifyInicio.Visible = true;
            this.notifyInicio.ShowBalloonTip(200);
        }

        private void coloresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormParametros parametros = new FormParametros();
            parametros.MdiParent = this;
            parametros.Show();

            parametros.tabControlParametros.SelectTab("tabPageColores");
            //parametros.tabControlParametros.SelectedTab("tabPageColores");

            this.parametrosToolStripMenuItem.Enabled = false;
            parametros.FormClosed += delegate { this.parametrosToolStripMenuItem.Enabled = true; };
        }

        private void ayudaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            String Dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            String DirCompleta = System.IO.Path.Combine(Dir, "PANTALLA INICIAL.htm");
            //MessageBox.Show(DirCompleta);
            this.helpProvider1.HelpNamespace = DirCompleta.Remove(0, 6);

            Help.ShowHelp(this, this.helpProvider1.HelpNamespace);
        }

        private void toolStripAyuda_Click(object sender, EventArgs e)
        {
            AcercaDe cuadro = new AcercaDe();
            cuadro.ShowDialog();

            
        }

        private void especieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormParametros parametros = new FormParametros();
            parametros.MdiParent = this;
            parametros.Show();

            parametros.tabControlParametros.SelectTab("tabPageEspecie");

            this.parametrosToolStripMenuItem.Enabled = false;
            parametros.FormClosed += delegate { this.parametrosToolStripMenuItem.Enabled = true; };
        }

        private void razasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormParametros parametros = new FormParametros();
            parametros.MdiParent = this;
            parametros.Show();

            parametros.tabControlParametros.SelectTab("tabPageRaza");

            this.parametrosToolStripMenuItem.Enabled = false;
            parametros.FormClosed += delegate { this.parametrosToolStripMenuItem.Enabled = true; };
        }

        
    }
}
