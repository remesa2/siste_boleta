﻿namespace Veterinaria_Primavera_2014
{
    partial class FormMascota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMascota));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtCelular = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtFijo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtNombres = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtApMaterno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtApPaterno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtRUT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureColor = new System.Windows.Forms.PictureBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtmpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBoxSexo = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.rdbAsexuado = new System.Windows.Forms.RadioButton();
            this.rdbMacho = new System.Windows.Forms.RadioButton();
            this.rdbHembra = new System.Windows.Forms.RadioButton();
            this.cmbRaza = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbEspecie = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNombreMascota = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btmIngresarAtencion = new System.Windows.Forms.Button();
            this.btnEliminarMascota = new System.Windows.Forms.Button();
            this.btnModificarMascota = new System.Windows.Forms.Button();
            this.btnIngresarMascota = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureColor)).BeginInit();
            this.groupBoxSexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtEmail);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtCelular);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TxtFijo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtNombres);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtApMaterno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtApPaterno);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtRUT);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(966, 119);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATOS PERSONALES DEL DUEÑO";
            // 
            // TxtEmail
            // 
            this.TxtEmail.Location = new System.Drawing.Point(469, 80);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(395, 20);
            this.TxtEmail.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(466, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Correo Electronico";
            // 
            // TxtCelular
            // 
            this.TxtCelular.Location = new System.Drawing.Point(305, 80);
            this.TxtCelular.Name = "TxtCelular";
            this.TxtCelular.Size = new System.Drawing.Size(138, 20);
            this.TxtCelular.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(302, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Celular";
            // 
            // TxtFijo
            // 
            this.TxtFijo.Location = new System.Drawing.Point(150, 80);
            this.TxtFijo.Name = "TxtFijo";
            this.TxtFijo.Size = new System.Drawing.Size(132, 20);
            this.TxtFijo.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(147, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefono Fijo";
            // 
            // TxtNombres
            // 
            this.TxtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtNombres.Location = new System.Drawing.Point(629, 38);
            this.TxtNombres.Name = "TxtNombres";
            this.TxtNombres.Size = new System.Drawing.Size(235, 20);
            this.TxtNombres.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(626, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombres";
            // 
            // TxtApMaterno
            // 
            this.TxtApMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApMaterno.Location = new System.Drawing.Point(469, 38);
            this.TxtApMaterno.Name = "TxtApMaterno";
            this.TxtApMaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApMaterno.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Apellido Materno";
            // 
            // TxtApPaterno
            // 
            this.TxtApPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApPaterno.Location = new System.Drawing.Point(305, 38);
            this.TxtApPaterno.Name = "TxtApPaterno";
            this.TxtApPaterno.Size = new System.Drawing.Size(138, 20);
            this.TxtApPaterno.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Apellido Paterno";
            // 
            // TxtRUT
            // 
            this.TxtRUT.Location = new System.Drawing.Point(150, 38);
            this.TxtRUT.Name = "TxtRUT";
            this.TxtRUT.Size = new System.Drawing.Size(132, 20);
            this.TxtRUT.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RUT";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Veterinaria_Primavera_2014.Properties.Resources.user91_2_;
            this.pictureBox1.Location = new System.Drawing.Point(18, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(107, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureColor);
            this.groupBox2.Controls.Add(this.cmbColor);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dtmpFechaNacimiento);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.groupBoxSexo);
            this.groupBox2.Controls.Add(this.cmbRaza);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cmbEspecie);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtNombreMascota);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(966, 124);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATOS DE LA MASCOTA";
            // 
            // pictureColor
            // 
            this.pictureColor.Location = new System.Drawing.Point(469, 63);
            this.pictureColor.Name = "pictureColor";
            this.pictureColor.Size = new System.Drawing.Size(138, 42);
            this.pictureColor.TabIndex = 20;
            this.pictureColor.TabStop = false;
            // 
            // cmbColor
            // 
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(305, 81);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(138, 21);
            this.cmbColor.TabIndex = 19;
            this.cmbColor.Text = "<<Elija>>";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(302, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Color Principal";
            // 
            // dtmpFechaNacimiento
            // 
            this.dtmpFechaNacimiento.Location = new System.Drawing.Point(18, 78);
            this.dtmpFechaNacimiento.Name = "dtmpFechaNacimiento";
            this.dtmpFechaNacimiento.Size = new System.Drawing.Size(264, 20);
            this.dtmpFechaNacimiento.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Fecha de Nacimiento";
            // 
            // groupBoxSexo
            // 
            this.groupBoxSexo.Controls.Add(this.pictureBox2);
            this.groupBoxSexo.Controls.Add(this.rdbAsexuado);
            this.groupBoxSexo.Controls.Add(this.rdbMacho);
            this.groupBoxSexo.Controls.Add(this.rdbHembra);
            this.groupBoxSexo.Location = new System.Drawing.Point(629, 16);
            this.groupBoxSexo.Name = "groupBoxSexo";
            this.groupBoxSexo.Size = new System.Drawing.Size(320, 82);
            this.groupBoxSexo.TabIndex = 15;
            this.groupBoxSexo.TabStop = false;
            this.groupBoxSexo.Text = "Sexo de la Mascota";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Veterinaria_Primavera_2014.Properties.Resources.dog50__1_;
            this.pictureBox2.Location = new System.Drawing.Point(196, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 68);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // rdbAsexuado
            // 
            this.rdbAsexuado.AutoSize = true;
            this.rdbAsexuado.Location = new System.Drawing.Point(118, 41);
            this.rdbAsexuado.Name = "rdbAsexuado";
            this.rdbAsexuado.Size = new System.Drawing.Size(72, 17);
            this.rdbAsexuado.TabIndex = 2;
            this.rdbAsexuado.TabStop = true;
            this.rdbAsexuado.Text = "Asexuado";
            this.rdbAsexuado.UseVisualStyleBackColor = true;
            // 
            // rdbMacho
            // 
            this.rdbMacho.AutoSize = true;
            this.rdbMacho.Location = new System.Drawing.Point(54, 41);
            this.rdbMacho.Name = "rdbMacho";
            this.rdbMacho.Size = new System.Drawing.Size(58, 17);
            this.rdbMacho.TabIndex = 1;
            this.rdbMacho.TabStop = true;
            this.rdbMacho.Text = "Macho";
            this.rdbMacho.UseVisualStyleBackColor = true;
            // 
            // rdbHembra
            // 
            this.rdbHembra.AutoSize = true;
            this.rdbHembra.Location = new System.Drawing.Point(54, 18);
            this.rdbHembra.Name = "rdbHembra";
            this.rdbHembra.Size = new System.Drawing.Size(62, 17);
            this.rdbHembra.TabIndex = 0;
            this.rdbHembra.TabStop = true;
            this.rdbHembra.Text = "Hembra";
            this.rdbHembra.UseVisualStyleBackColor = true;
            // 
            // cmbRaza
            // 
            this.cmbRaza.FormattingEnabled = true;
            this.cmbRaza.Location = new System.Drawing.Point(469, 34);
            this.cmbRaza.Name = "cmbRaza";
            this.cmbRaza.Size = new System.Drawing.Size(138, 21);
            this.cmbRaza.TabIndex = 14;
            this.cmbRaza.Text = "<<Elija>>";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(466, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Raza";
            // 
            // cmbEspecie
            // 
            this.cmbEspecie.FormattingEnabled = true;
            this.cmbEspecie.Location = new System.Drawing.Point(305, 34);
            this.cmbEspecie.Name = "cmbEspecie";
            this.cmbEspecie.Size = new System.Drawing.Size(138, 21);
            this.cmbEspecie.TabIndex = 12;
            this.cmbEspecie.Text = "<<Elija>>";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(302, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Especie";
            // 
            // txtNombreMascota
            // 
            this.txtNombreMascota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreMascota.Location = new System.Drawing.Point(18, 35);
            this.txtNombreMascota.Name = "txtNombreMascota";
            this.txtNombreMascota.Size = new System.Drawing.Size(264, 20);
            this.txtNombreMascota.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Nombre";
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(12, 347);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(966, 150);
            this.dataGridView1.TabIndex = 12;
            // 
            // btmIngresarAtencion
            // 
            this.btmIngresarAtencion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btmIngresarAtencion.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.add107__1_;
            this.btmIngresarAtencion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btmIngresarAtencion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btmIngresarAtencion.Location = new System.Drawing.Point(498, 283);
            this.btmIngresarAtencion.Name = "btmIngresarAtencion";
            this.btmIngresarAtencion.Size = new System.Drawing.Size(237, 39);
            this.btmIngresarAtencion.TabIndex = 11;
            this.btmIngresarAtencion.Text = "Ingresar / Consultar Atencion";
            this.btmIngresarAtencion.UseVisualStyleBackColor = false;
            this.btmIngresarAtencion.Click += new System.EventHandler(this.btmIngresarAtencion_Click);
            // 
            // btnEliminarMascota
            // 
            this.btnEliminarMascota.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEliminarMascota.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.delete30;
            this.btnEliminarMascota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEliminarMascota.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarMascota.Location = new System.Drawing.Point(741, 283);
            this.btnEliminarMascota.Name = "btnEliminarMascota";
            this.btnEliminarMascota.Size = new System.Drawing.Size(237, 39);
            this.btnEliminarMascota.TabIndex = 10;
            this.btnEliminarMascota.Text = "Eliminar Mascota";
            this.btnEliminarMascota.UseVisualStyleBackColor = false;
            this.btnEliminarMascota.Click += new System.EventHandler(this.btnEliminarMascota_Click);
            // 
            // btnModificarMascota
            // 
            this.btnModificarMascota.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnModificarMascota.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.pencil41;
            this.btnModificarMascota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModificarMascota.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarMascota.Location = new System.Drawing.Point(255, 283);
            this.btnModificarMascota.Name = "btnModificarMascota";
            this.btnModificarMascota.Size = new System.Drawing.Size(237, 39);
            this.btnModificarMascota.TabIndex = 9;
            this.btnModificarMascota.Text = "Modificar Datos de la Mascota";
            this.btnModificarMascota.UseVisualStyleBackColor = false;
            this.btnModificarMascota.Click += new System.EventHandler(this.btnModificarMascota_Click);
            // 
            // btnIngresarMascota
            // 
            this.btnIngresarMascota.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIngresarMascota.BackgroundImage = global::Veterinaria_Primavera_2014.Properties.Resources.open131_1_;
            this.btnIngresarMascota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnIngresarMascota.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresarMascota.Location = new System.Drawing.Point(12, 283);
            this.btnIngresarMascota.Name = "btnIngresarMascota";
            this.btnIngresarMascota.Size = new System.Drawing.Size(237, 39);
            this.btnIngresarMascota.TabIndex = 8;
            this.btnIngresarMascota.Text = "Ingresar Mascota";
            this.btnIngresarMascota.UseVisualStyleBackColor = false;
            this.btnIngresarMascota.Click += new System.EventHandler(this.btnIngresarMascota_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkRate = 100;
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // FormMascota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 509);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btmIngresarAtencion);
            this.Controls.Add(this.btnEliminarMascota);
            this.Controls.Add(this.btnModificarMascota);
            this.Controls.Add(this.btnIngresarMascota);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMascota";
            this.Text = "FormMascota";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMascota_FormClosed);
            this.Load += new System.EventHandler(this.FormMascota_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureColor)).EndInit();
            this.groupBoxSexo.ResumeLayout(false);
            this.groupBoxSexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox TxtEmail;
        public System.Windows.Forms.TextBox TxtCelular;
        public System.Windows.Forms.TextBox TxtFijo;
        public System.Windows.Forms.TextBox TxtNombres;
        public System.Windows.Forms.TextBox TxtApMaterno;
        public System.Windows.Forms.TextBox TxtApPaterno;
        public System.Windows.Forms.TextBox TxtRUT;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtmpFechaNacimiento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBoxSexo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton rdbAsexuado;
        private System.Windows.Forms.RadioButton rdbMacho;
        private System.Windows.Forms.RadioButton rdbHembra;
        private System.Windows.Forms.ComboBox cmbRaza;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbEspecie;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtNombreMascota;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btmIngresarAtencion;
        private System.Windows.Forms.Button btnEliminarMascota;
        private System.Windows.Forms.Button btnModificarMascota;
        private System.Windows.Forms.PictureBox pictureColor;
        private System.Windows.Forms.ComboBox cmbColor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnIngresarMascota;
    }
}