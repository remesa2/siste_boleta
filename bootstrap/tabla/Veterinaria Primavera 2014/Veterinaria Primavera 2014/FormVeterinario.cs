﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Veterinaria_Primavera_2014
{
    public partial class FormVeterinario : Form
    {
        public FormVeterinario()
        {
            InitializeComponent();
        }

        private void btnModificarVeterinario_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE MODIFICACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE INCLUIR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnEliminarVeterinario_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pagina en Construcion", "FORMULARIO DE ELIMINACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private Boolean ValidarVeterinario()
        {
            Boolean sw = true;


            if (this.TxtRUT.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtRUT, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtRUT, ""); }

            if (this.TxtApPaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApPaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApPaterno, ""); }

            if (this.TxtApMaterno.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtApMaterno, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtApMaterno, ""); }

            if (this.TxtNombres.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtNombres, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtNombres, ""); }

            if (this.TxtFijo.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtFijo, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtFijo, ""); }

            if (this.TxtCelular.Text.Trim() == "")///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtCelular, "No puede dejar este campo en blanco");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtCelular, ""); }

            if (!correo_correcto(this.TxtEmail.Text.Trim()))///trim elimina los espacios
            {
                this.errorProvider1.SetError(this.TxtEmail, "Direccion erronea");
                sw = false;
            }
            else { this.errorProvider1.SetError(this.TxtEmail, ""); }


            return sw;

        }



        #region CONTROLES CON KEYPRESS

        private void TxtRUT_KeyPress(object sender, KeyPressEventArgs e)
        {

            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar) || codigo == 8 || codigo == 45)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtFijo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar) || codigo == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (Char.IsDigit(e.KeyChar) || codigo == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtApPaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            int codigo = Convert.ToInt32(e.KeyChar);
            if (!Char.IsDigit(e.KeyChar) || codigo == 8 || codigo == 75 || codigo == 107)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion


        private Boolean correo_correcto(String correo)
        {
            String expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (System.Text.RegularExpressions.Regex.IsMatch(correo, expresion))
            {
                if (System.Text.RegularExpressions.Regex.Replace(correo, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }


        }

        private void btnIngresarVeterinario_Click(object sender, EventArgs e)
        {
            if (ValidarVeterinario())
            {
                MessageBox.Show("Datos Correctamente Ingresados", "INGRESO");
            }
        }

    }
}
