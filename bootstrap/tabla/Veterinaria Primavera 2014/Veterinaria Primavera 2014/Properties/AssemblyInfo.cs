﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general sobre un ensamblado se controla mediante el siguiente 
// conjunto de atributos. Cambie estos atributos para modificar la información
// asociada con un ensamblado.
[assembly: AssemblyTitle("Veterinaria Primavera 2014")]
[assembly: AssemblyDescription("Aplicacion de veterinaria diseñada, construida e implementada por los alumnos avanzados de la asignatura programacion III semestre primavera 2014")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("INACAP")]
[assembly: AssemblyProduct("Veterinaria Primavera 2014")]
[assembly: AssemblyCopyright("Copyright © FJIA  2014")]
[assembly: AssemblyTrademark("FJIA")]
[assembly: AssemblyCulture("")]

// Si establece ComVisible como false, los tipos de este ensamblado no estarán visibles 
// para los componentes COM. Si necesita obtener acceso a un tipo de este ensamblado desde 
// COM, establezca el atributo ComVisible como true en este tipo.
[assembly: ComVisible(false)]

// El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
[assembly: Guid("9f03ef2e-2cc0-47ff-88e6-d42705942ad3")]

// La información de versión de un ensamblado consta de los cuatro valores siguientes:
//
//      Versión principal
//      Versión secundaria 
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
// mediante el asterisco ('*'), como se muestra a continuación:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
