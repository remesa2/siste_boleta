<?php
session_start();

header('Content-Type: text/html; charset=UTF-8');
if($_SESSION['usuario'] == true)
 {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formulario para subir una imagen</title>
</head>

<body>

<br />
<h3><b><i><u>FORMULARIO PARA SUBIR UNA IMAGEN</u></i></b></h3>
<br /><br />
<form action="procesoCalen.php" enctype="multipart/form-data" method="post">
    <label for="imagen">Imagen:</label>
    <input id="imagen" name="imagen" size="30" type="file" />
    <br />
    <br />
    <label for="imagenGrande">Imagen Grande:</label>
    <input id="imagenGrande" name="imagenGrande" size="30" type="file" />
    <br />
    <br />
    <label for="cadenaTexto">Nombre de la Imagen:</label>
    <input type="text" name="cadenatexto" size="80" maxlength="300" width="500px"> 
    <br />
    <br />
    <label for="textoDia">Dia de la imagen:</label>
    <input type="text" name="textoDia" size="30" maxlength="100"> 
    <br />
    <br />
    <label for="textoMes">Mes de la imagen:</label>
    <input type="text" name="textoMes" size="30" maxlength="100"> 
    <br />
    <br />
    <label for="textoAno">Año de la imagen:</label>
    <input type="text" name="textoAno" size="30" maxlength="100"> 
    <br />
    <br />
   
    <input name="submit" type="submit" value="Guardar" />
</form>
<?
 }else{

  echo "No te has logueado para ver esta página";
  
  }
    
    ?>

</body>
</html>
