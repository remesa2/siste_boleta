<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];

$sw=isset($_GET['sw']) ? $_GET['sw']:"";
$clie=$_SESSION['cliente'] ? $_SESSION['cliente']:"";
$estado=isset($_GET['estado']) ? $_GET['estado']:"";
$subestado=isset($_GET['subestado']) ? $_GET['subestado']:"";
$pago=isset($_GET['pago']) ? $_GET['pago']:"";
$sql_reporte="";
   
   $rut=isset($_GET['cli_rut']) ? $_GET['cli_rut']:"";
    $nombre=isset($_GET['cli_nombre']) ? $_GET['cli_nombre']:"";
    
    $sql_reporte="SELECT 
    `do_tipo`, 
    `do_rut`,  
    deudor.deu_nombre, 
    `do_nro`, `do_emision`, 
    `do_vencimiento`, 
    `do_monto`, 
	`do_saldo`, 
    `do_descripcion`, 
    `do_descripcion2`, 
    `do_descripcion3`,
	`do_descripcion4`,
    `do_descripcion5`,
    `do_descripcion6`, 
    `do_descripcion7`,
    `do_descripcion8`,
    `do_descripcion9`,
    `do_descripcion10`,
    `do_descripcion11`,
    deudor.deu_fecha_ult,
	funcionario.FU_NOMBRE, 
    estado.es_nombre, 
    subestado.sub_nombre, 
    deudor.deu_fecha_prox, 
    cliente.cli_nombre, 
    estado_doc.estado_doc_nombre, 
    do_estado_fecha, do_fecha_carga, 
    ge_fecha_pagar,
    ge_abono,ge_usuario, 
    do_fecha_estado, 
    arbol_estado_documento.ead_nombre, 
    do_Observacion,
    deudor.deu_contacto_rut,
    deudor.deu_contacto_nombre

FROM sist_boleta.`deuda` 

LEFT JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente 
INNER JOIN sist_boleta.cliente ON cliente.cli_id=deuda.do_cliente 
INNER JOIN sist_boleta.estado ON estado.es_id=deudor.deu_estado 
INNER JOIN sist_boleta.subestado ON subestado.sub_id=deudor.deu_subestado 
INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO=deudor.deu_usuario 
INNER JOIN sist_boleta.estado_doc ON estado_doc.estado_doc_id=deuda.do_estado 
INNER JOIN sist_boleta.arbol_estado_documento ON arbol_estado_documento.ead_id=deuda.do_estado_doc 
WHERE `do_cliente`=$clie";
    
    if($estado!=""){
        $sql_reporte=$sql_reporte." AND deudor.deu_estado=$estado";
    }
    
    if($subestado!=""){
        $sql_reporte=$sql_reporte." AND deudor.deu_subestado=$subestado";
    }
    
    if($pago!=""){
        $sql_reporte=$sql_reporte." AND deuda.do_estado=$pago";
    }

    //echo $sql_reporte;




if($sw=="buscar"){
    $rut=isset($_GET['cli_rut']) ? $_GET['cli_rut']:"";
    $nombre=isset($_GET['cli_nombre']) ? $_GET['cli_nombre']:"";
    
    $sql_reporte="SELECT 
    `do_tipo`,
    `do_rut`,
    deudor.deu_nombre,
    `do_nro`, 
    `do_emision`, 
    `do_vencimiento`, 
    `do_monto`, 
    `do_saldo`, 
    `do_descripcion`, 
    `do_descripcion2`, 
    `do_descripcion3`, 
    `do_descripcion4`,
    `do_descripcion5`,
    `do_descripcion6`, 
    `do_descripcion7`,
    `do_descripcion8`,
    `do_descripcion9`,
    `do_descripcion10`,
    `do_descripcion11`,  
    funcionario.FU_NOMBRE, 
    deudor.deu_fecha_ult, 
    estado.es_nombre, 
    subestado.sub_nombre, 
    deudor.deu_fecha_prox, 
    cliente.cli_nombre, 
    estado_doc.estado_doc_nombre, 
    do_estado_fecha, 
    do_fecha_carga, 
    do_fecha_estado, 
    arbol_estado_documento.ead_nombre, 
    do_Observacion,
    deudor.deu_contacto_rut,
    deudor.deu_contacto_nombre
    
    FROM sist_boleta.`deuda` 
    
    INNER JOIN sist_boleta.deudor ON deudor.deu_rut=deuda.do_rut AND deudor.deu_cliente=deuda.do_cliente 
    INNER JOIN sist_boleta.cliente ON cliente.cli_id=deuda.do_cliente 
    INNER JOIN sist_boleta.estado ON estado.es_id=deudor.deu_estado 
    INNER JOIN sist_boleta.subestado ON subestado.sub_id=deudor.deu_subestado 
    INNER JOIN sist_boleta.funcionario ON funcionario.FU_CODIGO=deudor.deu_usuario 
    INNER JOIN sist_boleta.estado_doc ON estado_doc.estado_doc_id=deuda.do_estado  
    INNER JOIN sist_boleta.arbol_estado_documento ON arbol_estado_documento.ead_id=deuda.do_estado_doc 
    
    WHERE `do_cliente`=$clie";
    
    if($estado!=""){
        $sql_reporte=$sql_reporte." AND deudor.deu_estado=$estado";
    }
    
    if($subestado!=""){
        $sql_reporte=$sql_reporte." AND deudor.deu_subestado=$subestado";
    }
    
    if($pago!=""){
        $sql_reporte=$sql_reporte." AND deuda.do_estado=$pago";
    }
    
}

//echo $sql_reporte;


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-1.4.2.min.js"></script>
    <title>| Sistema Remesa</title>
    <script type="text/javascript" language="JavaScript">    

    $(document).ready(function(){
        var cliente='<?php echo $clie;?>';
        
        $("#estado").load("funciones/CombosDAtos.php?sw=estadoRep&cli="+cliente);
        $("#subestado").attr('disabled','disabled');
            $("#estado").change(function(event){
                    var gest = $("#estado").find(':selected').val();
                    $("#subestado").removeAttr('disabled');
                    $("#subestado").load("funciones/CombosDAtos.php?sw=subestadoRep&estado="+gest);
            });
    });
    
    function Buscar(){

          document.datos.sw.value='buscar';
          document.datos.submit();

      }
    
    </script>
      
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <a href="Export_exel_historico.php?sql=<?php echo $sql_reporte; ?>"><img src="bootstrap/img/export_to_excel.gif"> Exportar Archivo</a><br>
    <br>
    <div class="hero-unit">
        <form action="ReporteHistorico.php" method="get" name="datos">
        <table class="table table-condensed">
            <tr>
                <td>ESTADO PAGO</td>
                <td>
                    <label>
                        <select name="pago" id="pago">
                            <option value="" selected="selected" disabled="disabled">Todos</option>
                            <option value="0">Pendiente</option>
                            <option value="1">Pagado</option>
                            <option value="2">Suspendido</option>
							<option value="3">Abonado</option>
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
               <td>ESTADO</td>
               <td><label><select name="estado" id="estado"></select></label></td> 
            </tr>
            <tr>
                <td>SUB-ESTADO</td>
                <td><label><select name="subestado" id="subestado"></select></label></td>
                <td><label><input type="button" class="btn btn-info btn-large" onclick="Buscar()" value="BUSCAR"></label></td>
            <input type="hidden" name="sw">
            </tr>
        </table>
        </form>
    </div>
</div>
<div>
	<div>
        <!---tabla datos contactabilidad---->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="100" class="alert-danger">Detalle Cartera</th>
                </tr>
                <tr class="alert-success">
				    <th>CLIENTE</th>
                    <th>TIPO DOC</th>
                    <th>RUT</th>
                    <th>NOMBRE</th>
                    <th>NRO</th>
                    <th>EMISION</th>
                    <th>VECIMIENTO</th>
                    <th>MONTO</th>
                    <th>SALDO</th>
					<th>ESTADO PAGO</th>
					<th>FECHA PAGO</th>
                    <th>DESC1</th>
                    <th>DESC2</th>
                    <th>DESC3</th>
                    <th>DESC4</th>
					<th>DESC5</th>
					<th>DESC6</th>
					<th>DESC7</th>
					<th>DESC8</th>
					<th>DESC9</th>
					<th>DESC10</th>
                    <th>DESC11</th>
                    <th>USUARIO ASIGNADO</th>
					<th>FECHA DEUDOR</th>
                    <th>ESTADO DEUDOR</th>
                    <th>SUBESTADO DEUDOR</th>
					<th>FECHA GESTION</th>
					<th>USUARIO GESTION</th>
					<th>ESTADO GESTION</th>
                    <th>SUBESTADO GESTION</th>
					<th>TELEFONO</th>
					<th>OBSERVACION</th>
					<th>FECHA A PAGAR</th>
					<th>MONTO A PAGAR</th>
					<th>FECHA PROX</th>
                    <th>FECHA ESTADO</th>
                    <th>ESTADO DOC</th>
                    <th>OBS DOC</th>
                    <th>ADICIONAL DEUDOR 1</th>
                    <th>ADICIONAL DEUDOR 2</th>
					
                </tr>
            </thead>
            <tbody>
                <?php 
                if($sw=="buscar"){
                $reporte=mysqli_query($conn,$sql_reporte);
                while($fila=mysqli_fetch_object($reporte)){
                 ?>
                <tr>
		    <td><?php echo $fila->cli_nombre; ?></td>
                    <td><?php echo $fila->do_tipo; ?></td>
                    <td><?php echo $fila->do_rut; ?></td>
                    <td><?php echo $fila->deu_nombre; ?></td>
                    <td><?php echo $fila->do_nro; ?></td>
                    <td><?php echo $fila->do_emision; ?></td>
                    <td><?php echo $fila->do_vencimiento; ?></td>
                    <td><?php echo $fila->do_monto; ?></td>
                    <td><?php echo $fila->do_saldo; ?></td>
                    <td><?php echo $fila->estado_doc_nombre; ?></td>
                    <td><?php echo $fila->do_estado_fecha; ?></td>
                    <td><?php echo $fila->do_descripcion; ?></td>
                    <td><?php echo $fila->do_descripcion2; ?></td>
                    <td><?php echo $fila->do_descripcion3; ?></td>
                    <td><?php echo $fila->do_descripcion4; ?></td>
                    <td><?php echo $fila->do_descripcion5; ?></td>
                    <td><?php echo $fila->do_descripcion6; ?></td>
                    <td><?php echo $fila->do_descripcion7; ?></td>
                    <td><?php echo $fila->do_descripcion8; ?></td>
                    <td><?php echo $fila->do_descripcion9; ?></td>
                    <td><?php echo $fila->do_descripcion10; ?></td>
                    <td><?php echo $fila->do_descripcion11; ?></td>
                    <td><?php echo $fila->FU_NOMBRE; ?></td>
                    <td><?php echo $fila->deu_fecha_ult; ?></td>
                    <td><?php echo $fila->es_nombre; ?></td>
                    <td><?php echo $fila->sub_nombre; ?></td>
		      <?php 
            $sqlObs = "SELECT
                    estado.es_nombre,
                    subestado.sub_nombre,
                    observacion,
                    ge_fecha_pagar,
                    ge_abono,
                    FU_NOMBRE,
                    ge_telefono,
                    ge_fecha
                    
                FROM
                    gestion
                INNER JOIN funcionario ON ge_usuario = funcionario.FU_CODIGO
                INNER JOIN subestado ON subestado.sub_id = gestion.ge_subestado
                INNER JOIN estado ON estado.es_id = gestion.ge_estado
                WHERE
                    ge_rut = '".$fila->do_rut."'
                AND ge_cliente = '".$clie."'
                ORDER BY
                    ge_fecha DESC
                LIMIT 0,
                1 ";
			  $obsQuery=mysqli_query($conn,$sqlObs);
                if ($filaObs=mysqli_fetch_object($obsQuery))
				{?>
			<td><?php echo $filaObs->ge_fecha; ?></td>
			<td><?php echo $filaObs->FU_NOMBRE; ?></td>
			<td><?php echo $filaObs->es_nombre; ?></td>
			<td><?php echo $filaObs->sub_nombre; ?></td>
			 <td><?php echo $filaObs->ge_telefono; ?></td>
			 <td><?php echo $filaObs->observacion; ?></td>
			<td><?php echo $filaObs->ge_fecha_pagar; ?></td>
			<td><?php echo $filaObs->ge_abono; ?></td>
			  <?php  }
			  else
			  {?>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td> 
						<td></td>
                        <td></td>
					<?php
		       }
			  ?>
                    <td><?php echo $fila->deu_fecha_prox; ?></td>
                    <td><?php echo $fila->do_fecha_estado;        ?></td>
                    <td><?php echo $fila->ead_nombre;        ?></td>
                    <td><?php echo $fila->do_Observacion;        ?></td>
                    <td><?php echo $fila->deu_contacto_rut;        ?></td>
					<td><?php echo $fila->deu_contacto_nombre;        ?></td>
                    
                </tr>
                <?php } } ?>
                
            </tbody>
        </table> 
          
        
    </div>
</div>
</body>
</html>