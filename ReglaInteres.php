<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReglaInteres
 *
 * @author Remesa SPA
 */
class ReglaInteres {
    
    var $monto;
    var $interes;
    var $total;
    var $UF;
    
    
    function __construct() {
        
    }
    
    function Calcular($valor, $uf) {
        
        $this->monto=$valor;
        $this->UF=$uf;
        $cantida=$this->monto/$this->UF;
        
        if($cantida>50){
            $this->interes=0.03;
        }
        
        if($cantida<=50){
            $this->interes=0.06;
        }
        
        if($cantida<=10){
            $this->interes=0.09;
        }
        
        $this->total =  $this->monto*$this->interes;
        
        
        return $this->total;
    }
    
}
