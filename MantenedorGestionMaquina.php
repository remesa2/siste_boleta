<?php
include("funciones/f_usuario.php");
include("funciones/inicio.php");
include("subidaArchivos/xls/cargar.php");

validar_u();

$sw=$_POST['sw'];

if($sw=="cargar"){

$tipo_archivo=$_FILES['archivo']['type'];
$tamano_archivo = $_FILES['archivo']['size'];
$nombre_archivo ="";
$nombre_archivo ="Maquina_".date('Ymd_gis').".xls";

//echo $tipo_archivo." ".$nombre_archivo;

if (!(strpos($tipo_archivo, "excel") || strpos($tipo_archivo, "application/vnd.ms-excel") || strpos($tipo_archivo, "application/x-excel") || strpos($tipo_archivo, "application/x-msexcel"))) {
    echo "<script>alert('La extensión del archivo no es correcta.')</script>";
  echo '<script>location.href="MantenedorGestionMaquina.php";</script>';
}else{
    if (move_uploaded_file($_FILES['archivo']['tmp_name'], "subidaArchivos/archivos/".$nombre_archivo)){
		
		
		 $sql ="INSERT INTO historico_archivos (Tipo,Fecha,Hora,CodCartera,NombreArchivo,Estatus) 
        VALUES ('Maquina','".date("Y-m-d")."','".date("H:i:s")."','','".$nombre_archivo."','Precarga') ";
        $rres=mysql_query($sql);
		
		$cc=new cargar();
		$cc->cargarMaquina($nombre_archivo);
    }else{
     echo "<script>alert('Ocurrió algún error al subir el fichero. No pudo guardarse.')</script>";
     echo '<script>location.href="MantenedorGestionMaquina.php";</script>';
    }
}
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css3/bootstrap.css" rel="stylesheet">
	
<title>| Sistema Remesa</title>

<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="plugins/multiple/multiple-select.css" rel="stylesheet">
</head>

<body>
<?php include("componentes/header.php");?>
    
<div class="container">	
<h4>Selecccionar el Archivo de Gestiones Maquina</h4> <a href="componentes/plantillas/Interface_Maquina.xls"><img src="bootstrap/img/export_to_excel.gif"> Exportar Formato</a><br>
<legend>Cargar Gestiones Maquina</legend>

	<form name="archivo" class="well" action="MantenedorGestionMaquina.php" method="post" enctype="multipart/form-data">
    <input type="file" name="archivo" value="" /><br/>   
    <input class="btn btn-success" name="subir" type="submit" value="Subir Archivo de Gestiones">
    <input type="hidden" name="sw" value="cargar">
    </form>
	
	<div class="table-responsive"><a id="detalle"></a>

    <table class="table table-striped table-bordered .table-condensed progress">
        <thead>
            <th style="text-align:center">Id</th>
            <th style="text-align:center">Fecha</th>
            <th style="text-align:center">Hora</th>
            <th style="text-align:center">Archivo</th>
            <th style="text-align:center">Estatus</th>
            <th style="text-align:center; width: 15px;">Acciones</th>
        </thead>
        <tbody>
          <?php
            $sql = "SELECT *, DATE_FORMAT(Fecha, '%d/%m/%Y') as Fecha FROM historico_archivos
                    WHERE Tipo = 'Maquina' 
            order By historico_archivos.Fecha DESC, historico_archivos.Hora DESC";
            $cont=0;
            $res = mysql_query($sql);
            while ($regs = mysql_fetch_array($res)) 
              {$cont++;?>
           <tr>
                    <td valign="middle" align="center" style="text-align: center;"><?php echo $cont; ?></td>
                    <td valign="middle" align="center" style="text-align: center;"><?php echo $regs["Fecha"]; ?></td>
                    <td valign="middle" align="center" style="text-align: center;"><?php echo $regs["Hora"]; ?></td>
                    <td valign="middle" align="center" style="text-align: left;"><?php echo $regs["NombreArchivo"]; ?></td>
                    <td valign="middle" align="center"  style="text-align: center;"><?php echo $regs["Estatus"]; ?></td>
					<td valign="middle" align="center"  style="text-align: center;">
					<?php if($regs["Estatus"]=="Precarga"){ ?>
						<a href="ArchivoGestionMaquina.php?archivo=<?php echo $regs["NombreArchivo"]; ?>" class='btn btn-info'>Subir Archivo De Gestiones</a>
					<?php }else{ ?>
						<a  href="DetalleGestionMaquina.php?arch=<?php echo $regs["NombreArchivo"]; ?>" class='btn btn-success'>Ver Detalle de Carga</a>
						
					<?php } ?>
					</td>
            </tr>		
			
          <?php } ?>
        </tbody>
    </table>
  </div>
  
  

</div>


</body>
</html>