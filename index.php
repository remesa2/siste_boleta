<?php include("funciones/f_usuario.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">

    <title> | Sistema REMESA</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="bootstrap/css/sigin.css" rel="stylesheet">

    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


  <body onload="javascript:document.validar.user.focus();">

    <div class="container">

      <?PHP
      if (isset($_GET['msj'])){
      $error=$_GET['msj'];
      //echo "<div align='center' class='aviso'>Error: $error_login_ms[$error]</div>";
          echo "<div class='alert alert-error'><a class='close' data-dismiss='alert'>×</a><strong>Warning!</strong> Error: $error</div>";
      }
      ?>  
	  <center><img src="bootstrap/img/logo_remesa.png" height="400px" width="400px">

      <form class="form-signin" role="form" action="principal.php?nombre_funcion=v_usuario" method="post" name="validar">
         <h2 class="form-signin-heading">Ingrese sus datos</h2>
		 <input type="hidden" value="v_usuario" name="nombre_funcion" />
        <input type="text" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
        <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required>
        <select name="cliente" id="cliente" required>
            <?php
            $conn = conectar();
            $consulta = "SELECT * FROM sist_boleta.`cliente` WHERE cli_bloqueado='0' ORDER BY cli_nombre asc";
            $query = mysqli_query($conn,$consulta);
                echo '<option value="0" selected="selected" disabled="disabled">Seleccione</option>';
                while ($fila = mysqli_fetch_array($query)) {
                echo '<option value="'.$fila['cli_id'].'">'.$fila['cli_nombre'].'</option>';
                };
            ?>
        </select>
        <br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
		<br></br>
		<h5 class="form-signin-heading">version 3.0</h5>
      </form></center>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>