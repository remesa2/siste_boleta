# Actualización de Aplicación Sist_boletas PHP 8.*



## Descripción del Proyecto:

Se propone actualizar la aplicación Sist_boletas desarrollada en PHP a la versión
PHP 8, asegurando su compatibilidad y funcionalidad con esta nueva versión del
lenguaje.

# Detalle de Trabajo:

## Análisis de la Aplicación:

- [ ] Revisión del código existente para identificar áreas que requieran
actualización para cumplir con los estándares y cambios introducidos
en PHP 8.
- [ ] Evaluación de bibliotecas, extensiones y dependencias utilizadas en la
aplicación para garantizar su compatibilidad con PHP 8.

## Actualización de Código:

- [ ] Modificación del código PHP existente para hacer uso de las
características introducidas en PHP 8 y ajustar aquellas partes que
puedan generar errores o advertencias en la nueva versión.
- [ ] Optimización de la sintaxis y refactorización del código según las
mejores prácticas recomendadas para PHP 8.

## Pruebas y Depuración:

- [ ] Realización de pruebas exhaustivas para verificar la funcionalidad
correcta de la aplicación en un entorno PHP 8.
- [ ] Identificación y corrección de errores, advertencias o problemas de
rendimiento que puedan surgir durante el proceso de actualización.

## Documentación:

- [ ] Actualización de la documentación del código, incluyendo comentarios
y descripciones relevantes, para reflejar los cambios realizados y
facilitar su mantenimiento futuro.

## Entregables:

- [ ] Código fuente actualizado y optimizado para PHP 8.
- [ ] Informe de pruebas y validación de la aplicación en PHP 8.
- [ ] Documentación actualizada del código.


# Proyecto de Migración a PHP 8 - Carta Gantt

A continuación se presenta la carta Gantt para el proyecto de migración de código a PHP 8:

![Carta Gantt - Migración a PHP 8](Img/Gantt.png)

En esta carta Gantt, se muestran las tareas necesarias para completar la migración del código a PHP 8. Cada barra representa una tarea y su longitud indica la duración estimada de la misma. Los bloques de color pueden representar diferentes tipos de tareas o hitos importantes en el proyecto.

## Objetivos del Proyecto

El objetivo principal de este proyecto es actualizar el código existente a PHP 8, aprovechando las nuevas características y mejoras de rendimiento ofrecidas por esta versión del lenguaje de programación.

## Beneficios de la Migración a PHP 8

- Mejoras en el rendimiento del código.
- Utilización de nuevas características del lenguaje.
- Mayor seguridad y estabilidad.

## Planificación

- **Análisis del Código Actual:** Realizar una revisión exhaustiva del código existente para identificar las áreas que necesitan ser actualizadas para ser compatibles con PHP 8.
- **Actualización de la Sintaxis:** Modificar el código para utilizar las nuevas sintaxis y características introducidas en PHP 8.
- **Pruebas y Depuración:** Realizar pruebas exhaustivas para asegurarse de que el código actualizado funcione correctamente y sin errores.
- **Despliegue:** Implementar el código actualizado en el entorno de producción.

## Duración del Proyecto

El proyecto está programado para completarse en un período de 4 semanas, con revisiones regulares del progreso y ajustes según sea necesario.

La carta Gantt proporciona una visión general del cronograma del proyecto y las tareas a realizar en cada etapa. Es una herramienta útil para la gestión eficaz del tiempo y los recursos disponibles durante la migración a PHP 8.



## Entrega 02-04-2024

- **Repositorio de GITLAB:** https://gitlab.com/remesa2/siste_boleta
- **Planificación:**
	- [x] Crear cuenta de gitlab - 01-04-2024 - Terminado
	- [x] subir Codigo - 01-04-2024 - Terminado
	- [x] Crear ambiente de Pruebas locales - 01-04-2024 a 02-04-2024 - Terminado
	- [ ] Revision del codigo - 02-04-2024 a 14-04-2024 - En curso
	- [ ] Desarrollo carga Excel - 14-04-2024 a 19-04-2024 - Sin Avance
	- [ ] Subir al servidor - 20-04-2024 a 21-04-2024 - Sin Avance
	- [ ] Pruebas con el cliente - 22-04-2024 a 27-04-2024 - Sin Avance


## Entrega 10-04-2024

- **Repositorio de GITLAB Actualizado:** https://gitlab.com/remesa2/siste_boleta
- **Planificación Actualizado:**
	- [x] Crear cuenta de gitlab - 01-04-2024 - Terminado
	- [x] subir Codigo - 01-04-2024 - Terminado
	- [x] Crear ambiente de Pruebas locales - 01-04-2024 a 02-04-2024 - Terminado
	- [x] Revision del codigo - 02-04-2024 a 14-04-2024 - En curso
	- [ ] Desarrollo carga Excel - 14-04-2024 a 19-04-2024 - Sin Avance
	- [x] Subir al servidor - 10-04-2024 a 21-04-2024 - Adelantada
	- [x] Pruebas con el cliente - 10-04-2024 a 27-04-2024 - Adelantada

- **Carta Gantt Actualizada:**
		![Carta Gantt Actualizada - Migración a PHP 8](Img/Gantt2.png)

- **Observaciones:** Durante la fase de levantamiento del sistema y creación del entorno de pruebas, hemos identificado áreas de mejora en el funcionamiento del código. En primer lugar, hemos notado deficiencias relacionadas con la ausencia de archivos de texto, lo que ha resultado en enlaces a destinos que no existen. Además, hemos detectado disparidades en la estructura de la base de datos debido a la falta de ciertos campos en algunas tablas. Para abordar estas deficiencias, hemos procedido a crear los campos faltantes en las tablas correspondientes de la base de datos.

	Además, durante nuestra revisión, hemos observado que el código hace referencia a una tabla que no está presente en la base de datos. Esta tabla parece estar relacionada con la generación de informes. Para resolver esta discrepancia, es necesario investigar y corregir la lógica de la aplicación para asegurar que esté alineada con la estructura de la base de datos existente. En caso necesario, consideraremos la creación de la tabla requerida para mantener la integridad y coherencia del sistema en su totalidad.

	Con el objetivo de facilitar esta revisión conjunta con el cliente, hemos avanzado en la tarea de cargar el código en el servidor y llevar a cabo pruebas con su participación.
- **Despliegue de la Aplicación para revisión :** http://177.221.141.160/siste_boleta/

- **Actualización Mysql :**
	```sql
	ALTER TABLE subestado ADD COLUMN calificacion INT;
	ALTER TABLE subestado ADD COLUMN sub_monto INT;
	ALTER TABLE subestado ADD COLUMN sub_fecha INT;
	ALTER TABLE cliente ADD COLUMN cli_estado_doc INT;
	```
- **Solicitudes :**
	- Revisión de archivos ausentes
		- [ ] Administración -> Carga Deudores. (http://177.221.141.160/siste_boleta/upload_Deuda.php)
		- [ ] Administración -> Carga Documentos. (http://177.221.141.160/siste_boleta/upload_Deuda.php)
		- [ ] Administración -> Carga Pagos. (http://177.221.141.160/siste_boleta/upload_Pagos.php)
		- [ ] Administración -> Cargar Gestiones Maquina. (subidaArchivos/xls/cargar.php)
		- [ ] Administración -> Mantenedor Arbol de Documentos. (http://177.221.141.160/siste_boleta/AgregaEstadoDocumento.php)
		- [ ] Administración -> Mantenedor Usuarios. (http://177.221.141.160/siste_boleta/Usuarios.php)
		- [ ] Administración -> Mantenedor Archivo 200 y 600. (http://177.221.141.160/FalabellaGes/)
		- [ ] Gestión -> Gestionar Agenda. (./Agenda.php)
		- [ ] Gestión -> Gestionar (http://177.221.141.160/siste_boleta/DeudorDeudaGestion.php)
		- [ ] Resportes -> Contactabilidad Actual (http://177.221.141.160/siste_boleta/contactabilidad.php)
		- [ ] Intranet -> Buscar por Telefono (http://177.221.141.160/BuscarRut/BuscarTelefono.php)
		- [ ] Intranet -> Buscar por Cartera (http://177.221.141.160/BuscarRut/BuscarAsignado.php)
		- [ ] Certificados -> Certificados por Rut (http://177.221.141.160/siste_boleta/upload_Rut.php)
		- [ ] Certificados -> Certificados por Docuemnto (http://177.221.141.160/siste_boleta/upload_Docs.php)
		- [ ] Generador de Campañas -> Campañas Telefonica (http://177.221.141.160/siste_boleta/discadorCampagna2.php)
		
	- Revisión de la tabla faltante en la base de datos - TABLA: "arbol_estado_documento" (Decidir si se corregirá la lógica o se creará la tabla).
	- Utilizar el sistema cargado en el servidor para apoyarse : http://177.221.141.160/siste_boleta/



