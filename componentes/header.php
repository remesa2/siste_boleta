<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title></title>

    <!-- Bootstrap core CSS -->
    <!--<link href="./bootstrap/css/bootstrap.min3.css" rel="stylesheet">
    <!--<link href="./bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/css/navbar_fixed.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <style>
    .dropdownTest {
      position: relative;
      display: inline-block;
    }

    .dropdownTest-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 120px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }

    .dropdownTest-content a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
    }

    .dropdownTest:hover .dropdownTest-content {
      display: block;
    }
  </style>

  <body>

    <!-- Fixed navbar -->
    <!---<div class="navbar navbar-default navbar-fixed-top" role="navigation">---->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#"><img src="./bootstrap/img/logo_remesa.png" width="60"></a>
        <div class="navbar-collapse">
          <ul class="nav">
              <li class="active"><a href="./principal.php">Inicio</a></li>
              
              <?php 

                $perfil = $_SESSION['tipoP'];
                $usuario = $_SESSION['usuario'];
                $id_cliente = $_SESSION['cliente'];
                $conn = conectar();
                $sql_cli="SELECT * FROM sist_boleta.`cliente` WHERE `cli_id` =$id_cliente";
                $cli=mysqli_query($conn,$sql_cli);
                $cl=mysqli_fetch_assoc($cli);
                $nombre_cli=$cl['cli_nombre'];

                //perfil administrador
              if($perfil=="6"){?>
              
                <li class="dropdown ">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracion <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 
                      <li class="nav-header">Proceso de Cargas</li>
				              <!--- <li><a href="./upload_Camp.php">Subir Campaña</a></li>--->
				              <li><a href="./upload_Deudores.php">Cargar Deudores</a></li>
                      <li><a href="./upload_Deuda.php">Cargar Documentos</a></li>
                      <li><a href="./upload_Pagos.php">Cargar Pagos</a></li>
				              <li><a href="./MantenedorGestionMaquina.php">Cargar Gestiones Maquina</a></li>
                 
                      <li class="divider"></li>
                      <li class="nav-header">Mantenedores</li>
                      <!---<li><a href="./valores.php">Mantenedor Valores</a></li>--->
                      <li><a href="./ArbolGestion.php">Mantenedor Arbol de Gestion</a></li>
                      <li><a href="./AgregaEstadoDocumento.php">Mantenedor Arbol de Documentos</a></li>
				              <li><a href="./AsignacionCarteraEjecutivos.php">Mantenedor Asignaciones</a></li>
                      <li><a href="./AgregaClientes.php">Mantenedor Clientes</a></li>
                      <li><a href="./Usuarios.php">Mantenedor Usuarios</a></li>	
                      <li><a href="../../FalabellaGes/">Mantenedor Archivo 200 y 600</a></li>
                      <!--- <li><a href="../../BuscarRut/BuscarRutNuev.php">Buscar Rut</a></li>--->
                  
                  </ul>
              </li>

            <?php } ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./GestionarAgenda.php">Gestionar Agenda</a></li>
				        <li><a href="./PrioridadDetalle.php">Gestionar Al <?php echo date("d/m/Y") ?></a></li>
                <li class="divider"></li>
                <li class="nav-header">Busqueda</li>
                <li><a href="./BuscarRut.php">Por RUT</a></li>
                <li><a href="./BuscarDocumento.php">Por Documento</a></li>
                <li><a href="./BuscarNombre.php">Por Nombre Deudor</a></li>
               
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reportes <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="./contactabilidad.php">Contactabilidad Actual</a></li>
          				 <li><a href="./ReporteFechaCompromiso.php">Compromisos Realizados</a></li>
          				 <li><a href="./ReporteHistorico.php">Estado Actual de la Cartera</a></li>
          				 <li><a href="./ReporteGestiones.php">Gestiones Realizadas</a></li>
          				 <li><a href="./ReporteHistoricoTodos.php">Historico Gestiones (Todas)</a></li>
          				 <li><a href="./ReporteMejorGestion.php">Historico Mejor Gestion</a></li>
          				 <!--<li><a href="../../BuscarRut/BuscarRutNuev.php">Buscar Rut Intranet</a></li>-->
    				 </ul>
            </li>
			
			  <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Intranet <b class="caret"></b></a>
              <ul class="dropdown-menu">
                   <li><a href="http://192.168.1.211/intra/">Acceso Intranet</a></li>
				   
				   <li><a href="../../BuscarRut/BuscarTelefono.php">Buscar por Telefono</a></li>
				   <li><a href="../../BuscarRut/BuscarAsignado.php">Buscar por Cartera</a></li>
				   				 </ul>
              </li>
			

            <?php if($perfil==6){?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Certificados<b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li class="nav-header">Documentos</li>
                  <li><a href="./upload_Rut.php">Certificados por Rut</a></li>
                  <li><a href="./upload_Docs.php">Certificados por Documentos</a></li>
                  
              </ul>
              </li>
			  
			  
                <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Generador de Campañas<b class="caret"></b></a>
              <ul class="dropdown-menu">
                    <li><a href="./discadorCampagna2.php">Campañas Telefonica</a></li>
					<li><a href="./discadorCampagna.php">Campañas Mailing</a></li>
					<li><a href="./discadorCampagna.php">Campañas SMS</a></li>
					<li><a href="./discadorCampagna.php">Campañas IVR</a></li>
              </ul>
            </li>
            </ul>
            <?php }?>
          
          <ul class="nav pull-right">
              <!----- cliente -*------->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nombre_cli; ?><b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <?php
                  $conn = conectar();
                  $sql_clientes="SELECT `uc_cliente`, cliente.cli_nombre  FROM sist_boleta.`usuario_cliente` INNER JOIN sist_boleta.cliente ON cliente.cli_id=usuario_cliente.uc_cliente WHERE `uc_usuario`='$usuario' AND cliente.cli_bloqueado=0 ORDER BY cliente.cli_nombre ASC";
                  $perfil_cliente=mysqli_query($conn,$sql_clientes);
                  while($fila=mysqli_fetch_object($perfil_cliente)){
                  ?>
                  <li><a href="./principal.php?fun=session&id_se=<?php echo $fila->uc_cliente; ?>"><?php echo $fila->cli_nombre; ?></a></li>
                  <?php } ?>
              </ul>
            </li>  
              
            <!----- cliente -*------->
            <!--<li><a href="../intra" target="_blank">Intranet</a></li>-->
            <li class="divider-vertical"></li>
            <li><a href="./funciones/cerrar_sesion.php">SALIR</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar -->

   


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery.min.js"></script>
     <script src="./bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
