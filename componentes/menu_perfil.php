<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title></title>
 </head>
 <body>


<?php

$perfil = $_SESSION['tipoP'];

//perfil administrador
if($perfil=="6"){?>
	<li class="active"><a href="principal.php">Home</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informes<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li class="dropdown-header">Informes en Linea</li>
                <li><a href="#">Llamadas Y tiempo</a></li>
                <li><a href="#">% Avance</a></li>
                
                <li class="divider"></li>
                <li class="dropdown-header">Estadisticas</li>
                <li><a href="#">Cantidades</a></li>
                <li><a href="#">Efectividad</a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Acciones y Solicitudes<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li class="dropdown-header">Acciones</li>
                <li><a href="upload.php">Cargar Campaña</a></li>
                <li><a href="campana-asignar.php">Asignar Campaña</a></li>
                
                <li class="divider"></li>
                <li class="dropdown-header">Mantenedores</li>
                <li><a href="ManClientes.php">Clientes</a></li>
                <li><a href="Areas-Comunas.php">Areas-Comunas</a></li>
                <li><a href="#">Resultados</a></li>
            </ul>
        </li>
<?php }

//perfil ejecutivo
if($perfil=="0"){?>
	<li class="active"><a href="#">Home</a></li>
        <li><a href="#about">Campañas</a></li>
        <li><a href="#contact">Informes</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Acciones y Solicitudes<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li class="dropdown-header">Acciones</li>
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
            </ul>
        </li>
<?php } ?>

 </body>
</html>