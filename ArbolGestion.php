<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$sw="";
if (isset($_GET['sw']))
     $sw=$_GET['sw'];
$clie=$_SESSION['cliente'];

if($sw=="Nestado"){
    $estado=$_GET['Arbol_estado'];

    $conn = conectar();
    
    $sql_insert_estado="INSERT INTO `sist_boleta`.`estado` VALUES (NULL , '$estado');";
    mysqli_query($conn,$sql_insert_estado);
}


if($sw=="Nsubestado"){
    $estado=$_GET['Estado'];
    $subestado=$_GET['Arbol_subestado'];
    $cliente=$_GET['cliente'];
    $peso=$_GET['Peso'];
    $monto=$_GET['monto'];
    $fecha=$_GET['fecha'];

    $conn = conectar();

    $sql_insert_subestado="INSERT INTO `sist_boleta`.`subestado` VALUES (NULL , '$estado', '$subestado', '$peso', '$monto', '$fecha');";

    mysqli_query($conn, $sql_insert_subestado);

    
    $sql_id_sub="SELECT sub_id FROM sist_boleta.subestado WHERE sub_estado='$estado' AND sub_nombre='$subestado'";
    $info=mysqli_query($conn, $sql_id_sub);
    $nf=mysqli_fetch_assoc($info);
    $sub_id=$nf['sub_id'];
    
    $sql_arbol="INSERT INTO sist_boleta.arbol_cliente VALUES (NULL, '$clie', '$estado', '$sub_id');";
    mysqli_query($conn, $sql_arbol);
    
}




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">    
    <script src="bootstrap/js/bootstrap-modal.js"></script>
	<script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <title>| Sistema Remesa</title>
    <script type="text/javascript" >
    
     function Valida_estado(){

        var campo;
        var error=0;
        var errortxt='';

        campo=document.estado.Arbol_estado.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un estado\n';
        }

        ////fiinnnnn validacionn


        if(error==0){
          document.estado.sw.value='Nestado';
          document.estado.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
      
      
      function Valida_subestado(){

        var campo;
        var error=0;
        var errortxt='';
        
        campo=document.subestado.Estado.value;

        if(campo==0){
          error=1;
          errortxt=errortxt+'Debe seleccionar un estado\n';
        }

        campo=document.subestado.Arbol_subestado.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe agregar un subestado\n';
        }
        
        campo=document.subestado.cliente.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'la Accion no esta asociada a ningun cliente\n';
        }

        ////fiinnnnn validacionn


        if(error==0){
          document.subestado.sw.value='Nsubestado';
          document.subestado.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
    
    </script>

    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <a data-toggle="modal" href="#agrega-estado" class="btn btn-success btn-small">Agregar estado</a>
        <a data-toggle="modal" href="#agrega-subestado" class="btn btn-success btn-small">Agregar SubEstado</a><br /><br />
        <!---tabla datos contactabilidad---->
        <a id="editar"></a><table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="6" class="alert-danger">Arbol de Gestion</th>
                </tr>
                <tr class="alert-success">
                    <th>CLIENTE</th>
                    <th>ESTADO</th>
                    <th>SUB-ESTADO</th>
                    <th>PESO</th>
					<th>MONTO</th>
					<th>FECHA</th>
                    <th style="text-align: center;">EDITAR</th>
                    <th style="text-align: center;">ELIMINAR</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $conn = conectar();
                $sql_arbol="SELECT subestado.sub_id, cliente.cli_nombre, estado.es_nombre, subestado.sub_nombre, calificacion, sub_monto, sub_fecha FROM sist_boleta.`arbol_cliente` INNER JOIN sist_boleta.estado ON estado.es_id=arbol_cliente.`ac_estado` INNER JOIN sist_boleta.subestado ON subestado.sub_id=arbol_cliente.`ac_subestado` INNER JOIN sist_boleta.cliente ON cliente.cli_id=arbol_cliente.`ac_cliente`WHERE `ac_cliente`='$clie' ORDER BY es_nombre,sub_nombre ASC";
                //echo $sql_arbol;
                $arbol=mysqli_query($conn, $sql_arbol);
                while($fila=mysqli_fetch_object($arbol)){
                 ?>
                <tr>
                    <td><?= $fila->cli_nombre; ?></td>
                    <td><?= $fila->es_nombre; ?></td>
                    <td><?= $fila->sub_nombre; ?></td>
                    <td style="text-align: center;"><?= $fila->calificacion; ?></td>
					<td><?php if($fila->sub_monto==0){ echo "NO"; }else { echo "SI"; } ?></td>
					<td><?php if($fila->sub_fecha==0){ echo "NO"; }else { echo "SI"; } ?></td>
                    <td style="text-align: center;"><a  href="#" onclick="openFancybox('editar','<?= $fila->sub_id ?>')"><img src="bootstrap/img/edit.png" width="25"></a></td>
                    <td style="text-align: center;"><a  href="ArbolGestionDelete.php?codigo=<?= $fila->sub_id ?>"><img src="bootstrap/img/del.png" width="25"></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table> 
        
        <!-- MODAL agrega estado-->
          <div id="agrega-estado" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3>Nuevo Estado</h3>
            </div>
              <form class="well" name="estado" action="ArbolGestion.php" method="GET">
            <div class="modal-body">
                    
                        <label>Estado :</label>
                        <input type="text" class="span3" name="Arbol_estado" placeholder="Escribe Estado…">
                        <input type="hidden" name="sw">
                    
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="Valida_estado()" >Guardar</a>
              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
            </div>
              </form>
          </div>
        
        
         <!-- MODAL agrega subestado-->
          <div id="agrega-subestado" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3>Nuevo SubEstado</h3>
            </div>
              <form class="well" name="subestado" action="ArbolGestion.php" method="GET">
            <div class="modal-body">
                
                        <label>Seleccione Estado: <select class="pagesize" name="Estado">
                        <option selected="selected"  value="0" disabled="disabled">Seleccione</option>
                        <?php
                        $conn = conectar();

                        $sql_eva="SELECT * FROM  sist_boleta.`estado` WHERE 1";
                        $eva=mysqli_query($conn, $sql_eva);
                        while($reva=mysqli_fetch_assoc($eva)){

                        echo "<option value='".$reva['es_id']."'>".$reva['es_nombre']."</option>";

                        }
                        ?>
                        </select>
                        </label>
                    
                        <label>SubEstado :</label>
                        <input type="text" class="span3" name="Arbol_subestado" placeholder="Escribe Sub-Estado…">

                        <label>Ponderación: <select class="pagesize" name="Peso">
                        <option selected="selected"  value="0" disabled="disabled">Seleccione</option>
                        <?php
                        for ($i = 1; $i <= 100; $i++) {
                           echo "<option value='".$i."'>".$i."</option>";
                        }
                        ?>
                        </select>
						

						<label class="control-label">Solicita Monto:</label>
						<div class="controls">
							<label class="radio">
							<input type="radio" name="monto" id="monto1" value="1" checked>
								Si
							</label>
							<label class="radio">
							<input type="radio" name="monto" id="monto0" value="0">
								No
							</label>
						</div>
						
						<label class="control-label">Solicita Fecha:</label>
						<div class="controls">
							<label class="radio">
							<input type="radio" name="fecha" id="fecha1" value="1" checked>
								Si
							</label>
							<label class="radio">
							<input type="radio" name="fecha" id="fecha2" value="0">
								No
							</label>
						</div>
						
          
						
                        <input type="hidden" name="sw" id="sw">
                        <input type="hidden" name="cliente" id="cliente" value="<?php echo $clie ?>">
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="Valida_subestado()" >Guardar</a>
              <a href="#" class="btn btn-danger" data-dismiss="modal" >Cerrar</a>
            </div>
              </form>
          </div>

        
    </div>
</div>


<script type="text/javascript" src="bootstrap/fancyapps/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<script>
  $(document).ready(function(){

      $('.fancybox').fancybox();

      

      $("#editar").click(function() {
          $.fancybox.open({
              width : '92%',
              href : 'ArbolGestionEditar.php?codigo='+$("#editar").prop('title')+"&sw="+$("#sw").val()+"&clie="+$("#cliente").val(),
              type : 'iframe',
              padding : 1,
              scrolling: 'no',
              afterClose : function(){
                            document.location.replace('ArbolGestion.php');
                        },
              iframe : {
                  scrolling : 'no',
                  preload   : true
              }
                


          });

      });
  



});

  function openFancybox(id,codigo) {
    document.getElementById(id).title=codigo;
    document.getElementById(id).click();
}
</script>

</body>
</html>