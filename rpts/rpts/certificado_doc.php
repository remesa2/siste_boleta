<?php
define('FPDF_FONTPATH','../fpdf/font/');

require('../fpdf/WriteTag.php');

class PDF extends PDF_WriteTag
{

function Certificado($regs)
{
	
//Margenes
	$this->SetMargins(1,1);
	$this->Image('../logo.jpg',165,8,50,26);

	//COLOR AZUL
	$this->SetStyle("TIT1","Arial","BU",13,"0,0,0",0);
	$this->SetStyle("TIT2","Arial","N",11,"0,0,0",0);
	$this->SetStyle("TIT3","Arial","BU",12,"0,0,0",0);
	$this->SetStyle("P1","Arial","N",8,"0,0,0",0);
	$this->SetStyle("P2","Arial","N",8,"0,0,0",0);
	$this->SetStyle("P3","Arial","N",11,"0,0,0",0);
	$this->SetStyle("P4","Arial","N",7,"0,0,0",2);
	$this->SetStyle("P6","Arial","N",6,"0,0,0",2);
	$this->SetStyle("P5","Arial","B",7,"0,0,0",0);
	$this->SetStyle("P7","Arial","N",6,"0,0,0",0);
	$this->SetStyle("NEG1","Arial","N",15,"0,0,0",0);
	$this->SetFillColor(245,245,245);

	$this->SetXY(5,17);
	$this->WriteTag(204,3,"<TIT1>ANEXO COMPLEMENTARIO CERTIFICADO DE CASTIGO</TIT1>",0,'C',0);

	$this->SetXY(20,30);
	$this->WriteTag(204,3,"<TIT3>Antecedentes del Titular</TIT3>",0,'L',0);

	$this->SetXY(20,40);
	$this->WriteTag(204,3,"<TIT2>Rut Cliente: </TIT2>",0,'L',0);
	$this->SetXY(70,40);
	$this->WriteTag(204,3,"<P3>".$regs["do_rut"]."</P3>",0,'L',0);

	$this->SetXY(20,50);
	$this->WriteTag(204,3,"<TIT2>Nombre Cliente: </TIT2>",0,'L',0);
	$this->SetXY(70,50);
	$this->WriteTag(204,3,"<P3>".$regs["deu_nombre"]."</P3>",0,'L',0);

	$this->SetXY(20,60);
	$this->WriteTag(204,3,"<TIT2>Direcci�n: </TIT2>",0,'L',0);
	$this->SetXY(70,60);
	$this->WriteTag(204,3,"<P3>".$regs["dir_direccion"]."</P3>",0,'L',0);

	$this->SetXY(20,70);
	$this->WriteTag(204,3,"<TIT2>Comuna: </TIT2>",0,'L',0);
	$this->SetXY(70,70);
	$this->WriteTag(204,3,"<P3>".$regs["dir_comuna"]."</P3>",0,'L',0);



	$this->SetXY(20,82);
	$this->WriteTag(204,3,"<TIT3>Detalle de la deuda</TIT3>",0,'L',0);

	$this->SetXY(20,92);
	$this->WriteTag(204,3,"<TIT2>Numero de documento: </TIT2>",0,'L',0);
	$this->SetXY(70,92);
	$this->WriteTag(204,3,"<P3>".$regs["do_nro"]."</P3>",0,'L',0);

	$this->SetXY(20,102);
	$this->WriteTag(204,3,"<TIT2>Tipo de Servicio: </TIT2>",0,'L',0);
	$this->SetXY(70,102);
	$this->WriteTag(204,3,"<P3>".$regs["do_tipo"]."</P3>",0,'L',0);

	$this->SetXY(20,112);
	$this->WriteTag(204,3,"<TIT2>Total Deuda: </TIT2>",0,'L',0);
	$this->SetXY(70,112);
	$this->WriteTag(204,3,"<P3> $".number_format($regs["do_saldo"],0,"",".")."</P3>",0,'L',0);


	$this->SetXY(5,123);
	$this->WriteTag(204,3,"<TIT2>Hist�rico de acciones de cobranza realizadas</TIT2>",0,'C',0);

    $this->SetXY(20,130);
	$this->WriteTag(25,6,"<P1>ESTADO</P1>",1,'C',1);
	$this->SetXY(45,130);
	$this->WriteTag(25,6,"<P1>SUB-ESTADO</P1>",1,'C',1);
	$this->SetXY(70,130);
	$this->WriteTag(20,6,"<P1>FECHA</P1>",1,'C',1);
	$this->SetXY(90,130);
	$this->WriteTag(35,6,"<P1>USUARIO</P1>",1,'C',1);
	$this->SetXY(125,130);
	$this->WriteTag(20,6,"<P1>TELEFONO</P1>",1,'C',1);
	$this->SetXY(145,130);
	$this->WriteTag(50,6,"<P1>OBSERVACIONES</P1>",1,'C',1);
    
    $link = Conectarse();
    $sql = "SELECT
				estado.es_nombre AS Estado,
				subestado.sub_nombre AS SubEstado,
				DATE_FORMAT(gestion.ge_fecha,'%d/%m/%Y') AS Fecha,
				gestion.ge_telefono AS Telefono,
				gestion.observacion AS Observaciones,
				funcionario.FU_NOMBRE
				FROM
				gestion
				INNER JOIN estado ON gestion.ge_estado = estado.es_id
				INNER JOIN subestado ON gestion.ge_subestado = subestado.sub_id
				INNER JOIN funcionario ON gestion.ge_usuario = funcionario.FU_CODIGO
				WHERE gestion.ge_rut = '".$regs["do_rut"]."' ";
    $cons = mysql_query($sql,$link);
    $y=136; $alto=6;
    while ($data = mysql_fetch_array($cons)) {
    	if ($y >= 230)
		{
			$this->AddPage();
			$this->SetAutoPageBreak(true,10);
			$this->SetMargins(1,1);
            
            $y=20;
			$this->SetXY(20,$y);
			$this->WriteTag(25,6,"<P1>ESTADO</P1>",1,'C',1);
			$this->SetXY(45,$y);
			$this->WriteTag(25,6,"<P1>SUB-ESTADO</P1>",1,'C',1);
			$this->SetXY(70,$y);
			$this->WriteTag(20,6,"<P1>FECHA</P1>",1,'C',1);
			$this->SetXY(90,$y);
			$this->WriteTag(35,6,"<P1>USUARIO</P1>",1,'C',1);
			$this->SetXY(125,$y);
			$this->WriteTag(20,6,"<P1>TELEFONO</P1>",1,'C',1);
			$this->SetXY(145,$y);
			$this->WriteTag(50,6,"<P1>OBSERVACIONES</P1>",1,'C',1);
			$y+=6;
			
		}

    	
    	switch (true)
    	{
    		case (strlen($data["Observaciones"]) <= 20): $alto=6; break;
    		case (strlen($data["Observaciones"]) > 20 && strlen($data["Observaciones"]) <= 90): $alto=12; break;
    		case (strlen($data["Observaciones"]) > 90 && strlen($data["Observaciones"]) <= 130): $alto=21; break;
    		case (strlen($data["Observaciones"]) > 130 && strlen($data["Observaciones"]) <= 170): $alto=29; break;
    		case (strlen($data["Observaciones"]) > 170 && strlen($data["Observaciones"]) <= 210): $alto=34; break;
    		case (strlen($data["Observaciones"]) > 210 && strlen($data["Observaciones"]) <= 250): $alto=38; break;

    	}
	    
        
        //if ($alto==6)
    	//{
		    switch (true)
	    	{
	    		//case (strlen($data["SubEstado"]) <= 20): $alto=6; break;
	    		case (strlen($data["SubEstado"]) >= 20 && strlen($data["SubEstado"]) <= 40): 
										if ($alto<=12)
											$alto=18; 
										break;
				case (strlen($data["SubEstado"]) > 40 && strlen($data["SubEstado"]) <= 100): 
				                        if ($alto<18)
											$alto=21;
										break;
	    	}
    	//}
	 	$this->SetXY(20,$y-1); 
		$this->WriteTag(25,$alto,"<P1>".$data["Estado"]."</P1>",0,'C',0,1);
		$this->SetXY(20,$y);
		$this->WriteTag(25,$alto,"<P1></P1>",1,'C',0);
		/*if (strlen($data["SubEstado"]) >= 20 && strlen($data["SubEstado"]) <= 24)
		    $this->SetXY(45,$y+($alto/2));
		elseif (strlen($data["SubEstado"]) >= 1 && strlen($data["SubEstado"]) < 20)
            $this->SetXY(45,$y+($alto/2)-3);
		else
	        $this->SetXY(45,$y);*/

		switch ($alto)
		{
			 case 12: $this->SetXY(45,$y+2); break;
			 case 18: $this->SetXY(45,$y+4); break;
			 case 21: $this->SetXY(45,$y+8); break;
			 case 29: $this->SetXY(45,$y+11); break;
			 case 34: $this->SetXY(45,$y+14); break;
			 case 38: $this->SetXY(45,$y+17); break;
			 default: $this->SetXY(45,$y); break;
		}
		$this->MultiCell(25,4,trim($data["SubEstado"]),0,'C',0);
		$this->SetXY(45,$y);
		$this->WriteTag(25,$alto,"<P1></P1>",1,'C',0);
		$this->SetXY(70,$y-1);
		$this->WriteTag(20,$alto,"<P1>".$data["Fecha"]."</P1>",0,'C',0,1);
		$this->SetXY(70,$y);
		$this->WriteTag(20,$alto,"<P1></P1>",1,'C',0);
		$this->SetXY(90,$y-1);
		$this->WriteTag(35,$alto,"<P1>".$data["FU_NOMBRE"]."</P1>",0,'C',0,1);
		$this->SetXY(90,$y);
		$this->WriteTag(35,$alto,"<P1></P1>",1,'C',0);
		$this->SetXY(125,$y);
		$this->WriteTag(20,$alto,"<P1>".$data["Telefono"]."</P1>",0,'C',0);
		$this->SetXY(125,$y);
		$this->WriteTag(20,$alto,"<P1></P1>",1,'C',0);
		$this->SetXY(145,$y);
		$this->WriteTag(50,4,"<P1>".substr(limpiar($data["Observaciones"]),0,220)."</P1>",0,'J',0,1);
		$this->SetXY(145,$y);
		$this->WriteTag(50,$alto,"<P1></P1>",1,'C',0);
		$y+=$alto;



	}






	
}

	
	
}

function limpiar($string)
{
 
    $string = trim($string);
 
    $string = str_replace(array('�', '�', '�', '�', '�', '�', '�', '�', '�'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),$string);
 
    $string = str_replace(array('�', '�', '�', '�', '�', '�', '�', '�'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),$string);
 
    $string = str_replace(array('�', '�', '�', '�', '�', '�', '�', '�'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),$string);
 
    $string = str_replace(array('�', '�', '�', '�', '�', '�', '�', '�'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),$string);
 
    $string = str_replace(array('�', '�', '�', '�', '�', '�', '�', '�'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),$string);
 
    $string = str_replace(array('�', '�', '�', '�'),array('n', 'N', 'c', 'C',),$string);
 
    //Esta parte se encarga de eliminar cualquier caracter extra�o
    //$string = str_replace(array("\", '�', '�', "-", "~", '@', "|", "!", ""","�", "$", "%", "&", "/","(", ")", "?", "'", "�","�", "[", "^", "<code>", "]","+", "}", "{", "�", "�",">", "< ", ";", ",", ":","."),' ',$string); 
    $string = str_replace(array('�', '�', '-', '~', '@', '|', '!', '"','�', '$', '%', '&','(', ')', '?', "'",  '}', '{', '�', '�',">", "< ", ";", ",", ":",".", ""),' ',$string);
 
    return $string;
}


function Conectarse () {
	$link = mysql_connect("localhost", "wcs","wcs2018") or die('Could not connect: ' . mysql_error());   
	$res =	mysql_select_db("sist_boleta", $link);
	return $link;
}

function iif($exp,$true,$false) {
    if ($exp)
	  {return $true;}
	else
	  {return $false;}

}

function formatoFecha($fecha) {
    if ($fecha != "")
      {
        $fecha_con = substr($fecha,0,10);
        $fecha_conv = explode("-",  $fecha_con);
        $dia = $fecha_conv[2]; 
        $mes = $fecha_conv[1];
        $ano = $fecha_conv[0];  
        if (($fecha_conv[2]."/".$fecha_conv[1]."/".$fecha_conv[0]) != "00/00/0000")
            {return $fecha_conv[2]."/".$fecha_conv[1]."/".$fecha_conv[0];}
        else
            {return "";}
       }
    else
       {
        return "";
       }
}

function formatoFecha2($fecha) {
    if ($fecha != "")
      {
        $fecha_con = substr($fecha,0,10);
        $fecha_conv = explode("/",  $fecha_con);
        $dia = $fecha_conv[0]; 
        $mes = $fecha_conv[1];
        $ano = $fecha_conv[2];  
        return $ano."-".$mes."-".$dia;
       }
    else
       {
        return "0000-00-00";
       }
}


	//$fechaD = $_REQUEST["fechaD"];
	$nro = $_REQUEST["codHistorico"];
	$pdf= new PDF('P','mm','Letter');
	// 'ARMAR REPORTE
	$sql = "SELECT DISTINCT
			historico_archivos_detalles.CodHistorico,
			deuda.do_rut,
			deudor.deu_nombre,
			direcciones.dir_direccion,
			direcciones.dir_comuna,
			deuda.do_nro,
			deuda.do_tipo,
			deuda.do_saldo,
			deuda.do_monto
			FROM
			historico_archivos_detalles
			INNER JOIN deuda ON historico_archivos_detalles.ColumnaA = deuda.do_rut
			INNER JOIN deudor ON deuda.do_rut = deudor.deu_rut
			INNER JOIN direcciones ON deudor.deu_rut = direcciones.dir_rut
			WHERE CodHistorico = $nro  order by historico_archivos_detalles.CodHistoricoDetalle ASC";  //LIMIT 0,5
	$link = Conectarse();
	$query = mysql_query($sql,$link);
    while ($regs = mysql_fetch_array($query))
    {
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetMargins(1,1);
		$pdf->Certificado($regs);
		
	}
	
	$pdf->Output();
?>
