<?php
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
$clie=$_SESSION['cliente'];
$sql_reporte=$_GET['sql'];

header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("content-disposition: attachment;filename=ReporteCompromiso.xls");


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Supervisor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<div id="tabla" >
        <table cellpadding="0" cellspacing="0" border="1" class="" id="example3" align="left">
            <thead>
                <tr class="alert-success">
                    <th>RUT</th>
                    <th>NOMBRE</th>
					<th>CIUDAD</th>
					<th>FECHA</th>
					<th>TELEFONO</th>
					<th>ESTADO</th>
                    <th>SUB-ESTADO</th>
					<th>MONTO</th>
					<th>SALDO</th>
					<th>ESTADO DEUDA</th>
					<th>ABONO/LIQUIDACION</th>
					<th>OBSERVACIONES</th>					
                    <th>FECHA PROXIMA</th>
                    <th>FECHA PAGO</th>
					<th>USUARIO</th>
					<th>CLIENTE</th>
					<th>FECHA ESTADO</th>
					<th>ESTADO DOC</th>
					<th>OBS DOC</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                 $reporte=mysql_query($sql_reporte);
                 while($fila=mysql_fetch_object($reporte))
				 {
                 ?>
                 <tr>
				    <td><?php echo $fila->ge_rut ;          ?></td>
					<td><?php echo $fila->deu_nombre ;       ?></td>
					<td><?php echo $fila->dir_ciudad ;       ?></td>
					<td><?php echo $fila->ge_fecha;          ?></td>
					<td><?php echo $fila->ge_telefono;       ?></td>
					<td><?php echo $fila->es_nombre;         ?></td> 
					<td><?php echo $fila->sub_nombre;        ?></td>
					<td><?php echo $fila->do_monto;          ?></td>
					<td><?php echo $fila->do_saldo;          ?></td>
					<td><?php echo $fila->estado_doc_nombre; ?></td>
					<td><?php echo $fila->ge_abono;          ?></td> 
					<td><?php echo $fila->observacion;       ?></td> 
					<td><?php echo $fila->ge_fecha_prox;     ?></td>
					<td><?php echo $fila->ge_fecha_pagar;    ?></td>
                    <td><?php echo $fila->usuario;           ?></td>
					<td><?php echo $fila->cli_nombre;        ?></td>
					<td><?php echo $fila->do_fecha_estado;        ?></td>
					<td><?php echo $fila->ead_nombre;        ?></td>
					<td><?php echo $fila->do_Observacion;        ?></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table> 
        
    
    </div>
</body>
</html>