<?PHP
include("funciones/f_usuario.php");
include("funciones/inicio.php");
validar_u();
//$funcion=$_GET['fun'];
$nomb=isset($_GET['nombre']) ? $_GET['nombre']:"";
$sw=isset($_GET['sw']) ? $_GET['sw']:"";


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/img/favicon.ico">
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="./bootstrap/js/bootstrap-dropdown.js"></script>
    <title>| Sistema Remesa</title>
    <script type="text/javascript" language="JavaScript">
     function buscar(){

        var campo;
        var error=0;
        var errortxt='';

        campo=document.bqNOMBRE.nombre.value;

        if(campo==""){
          error=1;
          errortxt=errortxt+'Debe escribir el Nombre\n';
        }

        ////fiinnnnn validacionn


        if(error==0){
          document.bqNOMBRE.sw.value='buscar';
          document.bqNOMBRE.submit();

        }else{

          alert('Debe corregir:\n'+errortxt);
        }

      }
    </script>
    
  </head>

<body>
<div class="container">
    <?php include("componentes/header.php");?>
    <div class="hero-unit">
        <?php
            $err=isset($_GET['err']) ? $_GET['err']:"";
            if($err!=""){ ?>
              <div class='alert alert-error'><a class='close' data-dismiss='alert'>×</a><strong>Warning!</strong> Error: <?php echo $err; ?></div>  
        <?php } ?>
        <form class="well form-search" name="bqNOMBRE" action="BuscarNombre.php" method="GET">
            Ingrese Nombre :
            <input type="text" class="input-medium search-query" name="nombre">
            <input type="button" class="btn" value="Buscar" onclick="buscar()">
            <input type="hidden" name="sw">
        </form>
              
              
        <?php if($sw=="buscar"){    ?>  
              <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr class="alert-success">
                            <th>Rut</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $conn = conectar();

                        $sql_nombre="SELECT * FROM sist_boleta.`deudor` WHERE `deu_nombre` LIKE '%$nomb%' and deu_cliente='$id_cliente'";
                        $nombre=mysqli_query($conn,$sql_nombre);
                        while($fila=mysqli_fetch_object($nombre)){
                            $datos ="Busqueda por Nombre";
                        ?>
                        <tr>
                            <td><?php echo $fila->deu_rut; ?></td>
                            <td onclick="window.location = 'DeudorDeudaGestion.php?estado=bn&subestado=bn&dat=<?php echo $datos; ?>&rut=<?php echo $fila->deu_rut; ?>'"><?php echo $fila->deu_nombre; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table> 
        <?php } ?>
    </div>
</div>
</body>
</html>
